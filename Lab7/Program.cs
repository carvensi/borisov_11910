﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab7
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1");
            Console.WriteLine("Введите количество строк:");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите количество столбцов");
            int m = Convert.ToInt32(Console.ReadLine());
            int[,] array1 = new int[n, m];
            input(n, m, array1);
            Console.WriteLine("Какую строку вывести на экран?:");
            int n1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Какой столбец вывести на экран?:");
            int m1 = Convert.ToInt32(Console.ReadLine());
            output1(n1, m1, array1, n, m);
            Console.WriteLine();
            int sum = 0;
            if (n >= 3)
            {
                for (int i = 0; i < n; i++)
                {
                    sum = sum + array1[2, i];
                }
                Console.WriteLine("Сумма элементов 3 строки = " + sum);
            }
            else
            {
                Console.WriteLine("В массиве нет 3 строки");
            }
            Console.WriteLine("Для проверки");
            output2(array1, n, m);

            Console.WriteLine("Задание 2");
            Console.WriteLine("Введите количество строк:");
            int n2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите количество столбцов");
            int m2 = Convert.ToInt32(Console.ReadLine());
            int[,] array2 = new int[n2, m2];
            input(n2, m2, array2);
            Console.WriteLine("До:");
            output2(array2, n2, m2);
            resh2(array2, n2, m2);
            Console.WriteLine("После:");
            output2(array2, n2, m2);

            Console.WriteLine("Задание 3");
            int[,] array3 = new int[18, 36];
            input3(18, 36, array3);
            output2(array3, 18, 36);
            Console.WriteLine("Введите номер вагончика");
            int Vagon = Convert.ToInt32(Console.ReadLine());
            resh3(array3, Vagon - 1, 36);

            Console.WriteLine("Задание 4");
            Console.WriteLine("Введите количество строк:");
            int n4 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите количество столбцов");
            int m4 = Convert.ToInt32(Console.ReadLine());
            int[,] array4 = new int[n4, m4];
            input(n4, m4, array4);
            output2(array4, n4, m4);
            resh4(array4, n4, m4);
            resh44(array4, n4, m4);

            Console.WriteLine("Задание 5");
            Console.WriteLine("Введите количество строк:");
            int n5 = Convert.ToInt32(Console.ReadLine());
            int[,] array5 = new int[n5, n5];
            input(n5, n5,array5);
            output2(array5, n5, n5);
            resh5(array5, n5);

            Console.WriteLine("Задание 6");
            Console.WriteLine("Введите количество строк:");
            int n6 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите количество столбцов");
            int m6 = Convert.ToInt32(Console.ReadLine());
            int[,] array6 = new int[n6, n6];
            input(n6, m6, array6);
            output2(array6, n6, n6);
            resh6(array6,n6,m6);
        }
        static void input(int n, int m, int[,] array)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Console.WriteLine("Введите " + "(" + (i + 1) + "," + (j + 1) + ")" + " элемент массива");
                    array[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
        }
        static void output1(int n1, int m1, int[,] array, int n, int m)
        {
            Console.WriteLine(n1 + " строка:");
            for (int i = 0; i < n; i++)
                Console.Write(" {0}\t", array[n1 - 1, i]);
            Console.WriteLine();
            Console.WriteLine(m1 + " столбец:");
            for (int i = 0; i < m; i++)
                Console.Write(" {0}\t", array[i, m1 - 1]);
        }
        static void output2(int[,] array, int n2, int m2)
        {
            for (int i = 0; i < n2; i++)
            {
                for (int j = 0; j < m2; j++)
                    Console.Write(array[i, j] + " ");
                Console.WriteLine();
            }
        }
        static void resh2(int[,] array, int n2, int m2)
        {
            int x = 0;
            for (int i = 0; i < n2 / 2; i++)
            {
                for (int j = 0; j < m2; j++)
                {
                    x = array[i, j];
                    array[i, j] = array[n2 - i - 1, j];
                    array[n2 - 1 - i, j] = x;
                }
            }
        }
        static void input3(int n, int m, int[,] array)
        {
            Random rdn = new Random();
            Console.WriteLine("Заполняем массив рандомными числами...");
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    array[i, j] = rdn.Next(0, 2);
                }
            }
        }
        static void resh3(int[,] array, int n2, int m2)
        {
            int x = 0;
            for (int i = 0; i < m2; i++)
            {
                x = x + array[n2, i];
            }
            Console.WriteLine("Количество свободных мест: " + (36 - x));
        }
        static void resh4(int[,] array, int n, int m)
        {
            int a = 1000000000;
            int b = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    b = b + array[i, j];
                }
                if (b < a)
                {
                    a = b;
                }
                b = 0;
            }
            Console.WriteLine("Минимальная сумма элементов строки: " + a);
        }
        static void resh44(int[,] array, int n, int m)
        {
            int c = 0;
            int d = 0;
            for (int j = 0; j < n; j++)
            {
                for (int i = 0; i < m; i++)
                {
                    c = c + array[i, j];
                }
                if (d < c)
                {
                    d = c;
                }
                c = 0;
            }
            Console.WriteLine("Максимальная сумма элементов столбца: " + d);
        }
        static void resh5(int[,] array, int n)
        {
            int s = 0;
            int c = 0;
            for (int i = 0; i < n; i++)
            {
                s = s + array[i, i];
            }
            Console.WriteLine("a.Суммa элементов главной диагонали массива: " + s);

            for (int i = 0; i < n; i++)
            {
                if (array[i, n - i - 1] % 2 == 0)
                {
                    c = c + 1;
                }
            }
            Console.WriteLine("b.Количество четных элементов побочной диагонали: " + c);
        }
        static void resh6(int[,] array, int n, int m)
        {
            int[] array2 = new int[n];
            int nom = 0;
            int sum = 0;
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if ((array[j, i] % 2 == 0) && (array[j, i] >= 0))
                    {
                        sum = sum + array[j, i];
                    }
                }
                array2[nom] = sum;
                nom++;
                sum = 0;
            }
            Console.WriteLine("А) Одномернный массив:");
            for (int i = 0; i < n; i++)
            {
                Console.Write(array2[i] + " ");
            }

            int[] array3 = new int[m];
            int nom3 = 0;
            int max = 0;
            for (int i = 0; i < m; i++)
            {
                max = array[i, 0];
                for (int j = 0; j < n; j++)
                {
                    if (Math.Abs(array[i, j]) > max)
                    {
                        max = array[i, j];
                    }
                }
                array3[nom3] = max;
                nom3++;
            }
            Console.WriteLine();
            Console.WriteLine("А) Одномернный массив:");
            for (int i = 0; i < n; i++)
            {
                Console.Write(array3[i] + " ");
            }
            Console.WriteLine();
        }
    }
} 


