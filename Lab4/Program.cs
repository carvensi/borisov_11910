﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4
{
    class Program
    {
        static int Min(int a, int b)
        {
            if (a < b)
            {
                return a;
            }
            else
            {
                return b;
            }
        }
        static void Fxsin(int a, int b)
        {
            if (Math.Pow(a,3)-Math.Sin(a) > Math.Pow(b, 3) - Math.Sin(b))
            {
                Console.WriteLine("Максимальное значение функция принимает в точке a = " + a);
            }
            else
            {
                Console.WriteLine("Максимальное значение функция принимает в точке b = " + b);
            }
        }
        static int Fxsp(int a)
        {
            a = a / 10 % 10;
            return a;
        }
        static double Fxnat(double a)
        {
            return Math.Sqrt(a) + a;
        }
        static int Fxch(int ch)
        {
            if (ch % 2 == 0)
            {
                return ch / 2;
            }
            else
            {
                return 0;
            }
        }
        static int Fx5(int ch)
        {
            if (ch % 5 == 0)
            {
                return ch / 5;
            }
            else
            {
                return ch-1;
            }
        }
        static double Fy1(double a)
        {
            if (a >= 0.9)
            {
                return 1 / Math.Pow((0.1 + a), 2);
            }
            if (a >= 0 && a < 0.9)
            {
                return 0.2 * a + 0.1;
            }
            if (a < 0)
            {
                return Math.Pow(a, 2) + 0.2;
            }
            else
            {
                return 0;
            }
        }
        static double Fy2(double a)
        {
            if (Math.Abs(a) < 3)
            {
                return Math.Sin(a);
            }
            if (3 <= Math.Abs(a) && Math.Abs(a) <= 9)
            {
                return Math.Sqrt(Math.Pow(a, 2) + 1) / Math.Sqrt(Math.Pow(a, 2) + 5);
            }
            if (Math.Abs(a) >= 9)
            {
                return Math.Sqrt(Math.Pow(a, 2) + 1) - Math.Sqrt(Math.Pow(a, 2) + 5);
            }
            else
            {
                return 0;
            }
        }
        static double Fy3(double a, double b)
        {
            if (a == b)
            {
                return 1;
            }
            if (a > b)
            {
                return (a - b) / (a + b);
            }
            if (a < b)
            {
                return 0;
            }
            else
            {
                return 0;
            }
        }
        static double Fy4(double a)
        {
            if (Math.Abs(a) <= 0.1)
            {
                return Math.Pow(a, 3) - 0.1;
            }
            if (0.1 <= Math.Abs(a) && Math.Abs(a) <= 0.2)
            {
                return 0.2 * a - 0.1;
            }
            if (Math.Abs(a) >= 0.2)
            {
                return Math.Pow(a, 3) + 0.1;
            }
            else
            {
                return 0;
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Задания I");
            Console.WriteLine("Задание I.1");
            Console.WriteLine("Введите два числа");
            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            int z = Min(a * 3, b * 2) + Min((a - b), (a + b));
            Console.WriteLine(z);

            Console.WriteLine("Задание I.2");
            Console.WriteLine("Введите два числа");
            int c = Convert.ToInt32(Console.ReadLine());
            int d = Convert.ToInt32(Console.ReadLine());
            Fxsin(c, d);

            Console.WriteLine("Задание I.3");
            Console.WriteLine("Введите три числа");
            int e = Convert.ToInt32(Console.ReadLine());
            int f = Convert.ToInt32(Console.ReadLine());
            int g = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("z =" + (Fxsp(e) + Fxsp(f) - Fxsp(g)));

            Console.WriteLine("Задание I.4");
            Console.WriteLine("Введите три числа");
            int n1 = Convert.ToInt32(Console.ReadLine());
            int n2 = Convert.ToInt32(Console.ReadLine());
            int n3 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Выражение в условии " + (Fxnat(6) + Fxnat(13) - Fxnat(21)/2));
            Console.WriteLine("Выражение по введенным числам " + (Fxnat(n1) + Fxnat(n2) - Fxnat(n3) / 2));

            Console.WriteLine("Задание I.5");
            Console.WriteLine("Введите число");
            int ch = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("При числе 15: "+ Fxch(15)+" ,при вводе числа 10: " + Fxch(10) + ", при вводе числа x:"+ Fxch(ch));

            Console.WriteLine("Задание I.6");
            Console.WriteLine("Введите число");
            int ch5 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("При числе 15: " + Fx5(15) + " ,при вводе числа 9: " + Fx5(9) + ", при вводе числа x:" + Fx5(ch5));

            Console.WriteLine("Задания II");
            Console.WriteLine("Задание II.1");
            Console.WriteLine("Введите область значений (a, b): ");
            double a2 = Convert.ToDouble(Console.ReadLine());
            double b2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Введите шаг h: ");
            double h = Convert.ToDouble(Console.ReadLine());
            for (double i = a2; i <= b2; i += h)
            {
                Console.WriteLine("y = {0}", Fy1(i));
            }

            Console.WriteLine("Задание II.2");
            for (double i = a2; i <= b2; i += h)
            {
                Console.WriteLine("y = {0}", Fy2(i));
            }

            Console.WriteLine("Задание II.3");
            for (double i = a2; i <= b2; i += h)
            {
                Console.WriteLine("y = {0}", Fy3(i,a2));
            }

            Console.WriteLine("Задание II.4");
            for (double i = a2; i <= b2; i += h)
            {
                Console.WriteLine("y = {0}", Fy4(i));
            }
        }
    }
}

