﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Lab10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1");
            Console.WriteLine("Введите сообщение: ");
            string soob = Console.ReadLine();
            Console.WriteLine("Введите слово:");
            string slov = Console.ReadLine();
            Regex regex = new Regex(slov);
            MatchCollection matches = regex.Matches(soob);
            if (matches.Count > 0)
            {
                Console.WriteLine("Содержится");
            }
            else
            {
                Console.WriteLine("Совпадений не найдено");
            }

            Console.WriteLine("Задание 2");
            Console.WriteLine("Введите длину слова:");
            int length = Convert.ToInt32(Console.ReadLine());
            Regex regex2 = new Regex(@"\b\w{"+ length + @"}\b");
            MatchCollection matches2 = regex2.Matches(soob);
            if (matches.Count > 0)
            {
                foreach (Match match in matches2)
                    Console.WriteLine(match.Value);
            }
            else
            {
                Console.WriteLine("Совпадений не найдено");
            }

            Console.WriteLine("Задание 3");
            Regex regex3 = new Regex(@"\b[A-Z,А-Я]\w*\b");
            MatchCollection matches3 = regex3.Matches(soob);
            if (matches3.Count > 0)
            {
                foreach (Match match in matches3)
                    Console.WriteLine(match.Value);
            }
            else
            {
                Console.WriteLine("Совпадений не найдено");
            }

            Console.WriteLine("Задание 4");
            string target = "";
            Regex regex4 = new Regex("[-.?!)(,:]");
            string result = regex4.Replace(soob, target);
            Console.WriteLine(result);

            Console.WriteLine("Задание 5");
            string target5 = "...";
            Regex regex5 = new Regex("[a-zA-Z]+");
            string result5 = regex5.Replace(soob, target5);
            Console.WriteLine(result5);

        }
    }
}
