﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Задачи I");
            Console.WriteLine("Введите номер оператора цикла(1,2,3) для 1 задачи");
            int c = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите номер оператора цикла(1,2,3) для 2 задачи");
            int f = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите номер оператора цикла(1,2,3) для 3 задачи");
            int k = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите номер оператора цикла(1,2,3) для 4 задачи");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите номер оператора цикла(1,2,3) для 5 задачи");
            int p = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите номер оператора цикла(1,2,3) для 6 задачи");
            int tr = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите номер оператора цикла(1,2,3) для 7 задачи");
            int rz = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите номер оператора цикла(1,2,3) для 8 задачи");
            int st = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите номер оператора цикла(1,2,3) для 9 задачи");
            int pov = Convert.ToInt32(Console.ReadLine());
            int i = 60;
            
            switch (c)
            {
                case 1:
                    {
                        Console.WriteLine("Задача 1");
                        while (i >=10)
                        {
                            Console.WriteLine(i);
                            i -= 2;
                        }
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Задача 1");
                        do
                        {
                            Console.WriteLine(i);
                            i -= 2;
                        } while (i >=10);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Задача 1");
                        for (int b = 60; b>=10; b-=2)
                        {
                            Console.WriteLine(b);
                        }
                            break;
                    }
            }
            switch (f)
            {
                case 1:
                    {
                        Console.WriteLine("Задача 2");
                        int j = 1;
                        while (j <= 10)
                        {
                            Console.WriteLine(j+" фунтов" + "  " + j*453+ " Килограмм");
                            j ++;
                        }
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Задача 2");
                        int j = 1;
                        do
                        {
                            Console.WriteLine(j + " фунтов" + "  " + j * 453 + " Килограмм");
                            j++;
                        } while (j <= 10);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Задача 2");
                        for (int j = 1; j <= 10; j++)
                        {
                            Console.WriteLine(j + " фунтов" + "  " + j * 453 + " Килограмм");
                        }
                        break;
                    }
            }
            switch (k)
            {
                case 1:
                    {
                        Console.WriteLine("Задача 3");
                        Console.WriteLine("Введите A и B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        while (b>=a)
                        {
                            Console.WriteLine(Math.Pow(b,3));
                            b-=1;
                        }
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Задача 3");
                        Console.WriteLine("Введите A и B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        do
                        {
                            Console.WriteLine(Math.Pow(b, 3));
                            b -= 1;
                        } while (b >= a);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Задача 3");
                        Console.WriteLine("Введите A и B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        for (;b>=a;b--)
                        {
                            Console.WriteLine(Math.Pow(b, 3));
                        }
                        break;
                    }
            }
            switch (x)
            {
                case 1:
                    {
                        Console.WriteLine("Задача 4");
                        Console.WriteLine("Введите A, B и цифру ");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        int chislo = Convert.ToInt32(Console.ReadLine());
                        while (b >= a)
                        {
                            if (a%10==chislo)
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        }
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Задача 4");
                        Console.WriteLine("Введите A, B и цифру ");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        int chislo = Convert.ToInt32(Console.ReadLine());
                        do
                        {
                            if (a % 10 == chislo)
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        } while (b >= a);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Задача 4");
                        Console.WriteLine("Введите A, B и цифру ");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        int chislo = Convert.ToInt32(Console.ReadLine());
                        for (; b >= a; a++)
                        {
                            if (a % 10 == chislo)
                            {
                                Console.WriteLine(a);
                            }
                        }
                        break;
                    }
            }
            switch (p)
            {
                case 1:
                    {
                        Console.WriteLine("Задача 5");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        
                        while (b >= a)
                        {
                            if (a % 2 == 0)
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        }
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Задача 5");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        do
                        {
                            if (a % 2 == 0)
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        } while (b >= a);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Задача 5");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        for (; b >= a; a++)
                        {
                            if (a % 2 == 0)
                            {
                                Console.WriteLine(a);
                            }
                        }
                        break;
                    }
            }
            switch (tr)
            {
                case 1:
                    {
                        Console.WriteLine("Задача 6");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());

                        while (b >= a)
                        {
                            if (a % 3 == 0)
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        }
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Задача 6");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        do
                        {
                            if (a % 3 == 0)
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        } while (b >= a);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Задача 6");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        for (; b >= a; a++)
                        {
                            if (a % 3 == 0)
                            {
                                Console.WriteLine(a);
                            }
                        }
                        break;
                    }
            }
            switch (rz)
            {
                case 1:
                    {
                        Console.WriteLine("Задача 7");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());

                        while (b >= a)
                        {
                            if (a % 10 != a / 10)
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        }
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Задача 7");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        do
                        {
                            if (a % 10 != a / 10)
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        } while (b >= a);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Задача 7");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        for (; b >= a; a++)
                        {
                            if (a % 10 != a / 10)
                            {
                                Console.WriteLine(a);
                            }
                        }
                        break;
                    }
            }
            switch (st)
            {
                case 1:
                    {
                        Console.WriteLine("Задача 8");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());

                        while (b >= a)
                        {
                            if ((a / 10 == a % 10) || ((a / 10) + 1 == a % 10)|| (a / 10 == (a % 10)+1))
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        }
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Задача 8");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        do
                        {
                            if ((a / 10 == a % 10) || ((a / 10) + 1 == a % 10) || (a / 10 == (a % 10) + 1))
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        } while (b >= a);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Задача 8");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        for (; b >= a; a++)
                        {
                            if ((a / 10 == a % 10) || ((a / 10) + 1 == a % 10) || (a / 10 == (a % 10) + 1))
                            {
                                Console.WriteLine(a);
                            }
                        }
                        break;
                    }
            }
            switch (pov)
            {
                case 1:
                    {
                        Console.WriteLine("Задача 9");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());

                        while (b >= a)
                        {
                            if ((a / 100 == (a / 10)%10) || ((a / 10) % 10 == a % 10) || (a / 100 == a % 10)||((a / 100 == a % 10) && ((a / 10) % 10) == a % 10))
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        }
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Задача 9");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        do
                        {
                            if ((a / 100 == (a / 10) % 10) || ((a / 10) % 10 == a % 10) || (a / 100 == a % 10) || ((a / 100 == a % 10) && ((a / 10) % 10) == a % 10))
                            {
                                Console.WriteLine(a);
                            }
                            a++;
                        } while (b >= a);
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Задача 9");
                        Console.WriteLine("Введите A, B");
                        int a = Convert.ToInt32(Console.ReadLine());
                        int b = Convert.ToInt32(Console.ReadLine());
                        for (; b >= a; a++)
                        {
                            if ((a / 100 == (a / 10) % 10) || ((a / 10) % 10 == a % 10) || (a / 100 == a % 10) || ((a / 100 == a % 10) && ((a / 10) % 10) == a % 10))
                            {
                                Console.WriteLine(a);
                            }
                        }
                        break;
                    }
            }
            Console.WriteLine("Задачи II");
            Console.WriteLine("Задача 1");
            for (int a = 1; a <= 4; a++)
            {
                for(int b = 1; b <= 6; b++)
                {
                    Console.Write(a+"   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 2");
            for (int a = 1; a <= 4; a++)
            {
                for (int b = 1; b <= 10; b++)
                {
                    Console.Write(b + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 3");
            for (int a = 1; a <= 5; a++)
            {
                for (int b = -10; b <= 12; b++)
                {
                    Console.Write(b + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 4");
            for (int a = 0; a <= 30; a+=10)
            {
                for (int b = 41+a; b <= 50+a; b++)
                {
                    Console.Write(b + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 5");
            for (int a = 1; a <= 5; a ++)
            {
                for (int b = 1; b <= a; b++)
                {
                    Console.Write(5 + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 6");
            for (int a = 1; a <= 5; a++)
            {
                for (int b = 5; b >= a; b--)
                {
                    Console.Write(1 + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 7");
            for (int a = 1; a <= 5; a++)
            {
                for (int b = 1; b <= a; b++)
                {
                    Console.Write(a + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 8");
            for (int a = 6; a <= 10; a++)
            {
                for (int b = 10; b >= a; b--)
                {
                    Console.Write(a + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 9");
            for (int a = 7; a >= 3; a--)
            {
                for (int b = 7; b >= a; b--)
                {
                    Console.Write(a + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 10");
            for (int a = 8; a >= 4; a--)
            {
                for (int b = 4; b <= a; b++)
                {
                    Console.Write(a + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 11");
            for (int a = 1; a <= 5; a++)
            {
                for (int b = 1; b <= a; b++)
                {
                    Console.Write(b + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 12");
            for (int a = 0; a <= 4; a++)
            {
                for (int b = 1+a; b >= 1; b--)
                {
                    Console.Write(b + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 13");
            for (int a = 4; a >= 0; a--)
            {
                for (int b = 0; b <= a; b++)
                {
                    Console.Write(b + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 14");
            for (int a = 0; a <= 4; a++)
            {
                for (int b = 4-a; b >= 0; b--)
                {
                    Console.Write(b + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 15");
            for (int a = 1; a <= 5; a++)
            {
                for (int b = 1; b <= a; b++)
                {
                    Console.Write(a + "   ");
                }
                Console.WriteLine();
                for (int b = 1; b <= a; b++)
                {
                    Console.Write("0" + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 16");
            for (int a = 8; a > 4; a--)
            {
                for (int b = 8; b >= a; b--)
                {
                    Console.Write(a + "   ");
                }
                Console.WriteLine();
                for (int b = 8; b >= a; b--)
                {
                    Console.Write(a-1 + "   ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("Задача 17");
            for (int a = 1; a <= 4; a++)
            {
                for (int b = 1; b <= a; b++)
                {
                    Console.Write(a + "   ");
                }
                Console.WriteLine();
                for (int b = 1; b <= a; b++)
                {
                    Console.Write(a + 5 + "   ");
                }
                Console.WriteLine();
            }
            //Console.WriteLine("Задача 18");
            //for (int a = 9; a >= 6; a--)
            //{
              //  for (int b = 9; b >= a; b--)
                //{
                  //  Console.Write(a + "   ");
                //}
                //Console.WriteLine();
                //for (int b = 10; b >= a; b--)
                //{
                 //   Console.Write(a - 5 + "   ");
                //}
                //Console.WriteLine();
           // }
        }
    }
}
