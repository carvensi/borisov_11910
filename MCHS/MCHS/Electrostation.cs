﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MCHS
{
    /// <summary>
    /// Электростнация, источник события "пожар"
    /// </summary>
    class Electrostation
    {
        /// <summary>
        /// Событие пожар
        /// </summary>
        public Action<Electrostation, FireParams> FireAlarm;

        private string _name;
        public string Name { get { return _name; } }
        public Electrostation(string electName)
        {
            _name = electName;
        }

        /// <summary>
        /// Температура электростанции
        /// </summary>
        private int _temp;
        public int Temp
        {
            get { return _temp; }
            set { _temp = value; }
        }
        /// <summary>
        /// Пожар на электростанции
        /// </summary>
        public void FireAlarming(FireParams param)
        {
            if (FireAlarm != null)
                FireAlarm(this, param);
        }
    }
}
