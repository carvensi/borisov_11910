﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MCHS
{
    class FireRunner
    {
        public void Run()
        {
            Electrostation elect = new Electrostation("Чернобль");
            FireParams param = new FireParams();
            TurnOn turner = new TurnOn();
            //Станция не включена
            elect.FireAlarming(param);
            //Включаем станцию
            turner.TurnOnStation(elect);
            elect.FireAlarming(param);
        }
    }
}
