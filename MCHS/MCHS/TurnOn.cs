﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MCHS
{
    class TurnOn
    {
        public void TurnOnStation(Electrostation elect)
        {
            elect.FireAlarm += Alarm;
        }
        private void Alarm(Electrostation elect, FireParams param)
        {
            Console.WriteLine("Критическая температура станции " + elect.Name + " " + param.AlarmTemp);
            while (elect.Temp != param.AlarmTemp)
            {
                Console.WriteLine("Станция " + elect.Name + " нагревается," + " её температура " + elect.Temp);
                elect.Temp += 10;
                Thread.Sleep(3000);
            }
            Console.WriteLine(" Станция " +
                elect.Name + " перенагрелась! " +
                " Её температура " + param.AlarmTemp + "!"+" МЧС и пожарные уже едут! ");
        }
    }
}
