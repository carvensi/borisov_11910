﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KR1Var3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Вариант 3
            Console.WriteLine("Введите значения A и B, параметр Op");
            double a = Convert.ToDouble(Console.ReadLine());
            double b = Convert.ToDouble(Console.ReadLine());
            int op = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(Calc(a, b, op));

            double x1, x2 = 0;
            Console.WriteLine("Уравнение 4x2-5x+1=0:");
            if (Ur(4, -5, 1, out x1, out x2) == 0)
            {
                Console.WriteLine("Нет");
            }
            else
            {
                Console.WriteLine(Ur(4, -5, 1, out x1, out x2));
                Console.WriteLine(x1);
            }
            Console.WriteLine("Уравнение 13x2+2x+5=0:");
            if (Ur(13, 2, 5, out x1, out x2) == 0)
            {
                Console.WriteLine("Нет");
            }
            else
            {
                Console.WriteLine(Ur(13, 2, 5, out x1, out x2));
                Console.WriteLine(x1);
            }
            Console.WriteLine("Уравнение x2-8x+4=0:");
            if (Ur(1, -8, 4, out x1, out x2) == 0)
            {
                Console.WriteLine("Нет");
            }
            else
            {
                Console.WriteLine(Ur(1, -8, 4, out x1, out x2));
                Console.WriteLine(x1);
            }
            // Доп.задание
            Console.WriteLine("Введите два числа:");
            int a1 = Convert.ToInt32(Console.ReadLine());
            int a2 = Convert.ToInt32(Console.ReadLine());
            Pal(a1, a2);
        }
        static double Calc(double a,double b,int op)
        {
            switch (op)
            {
                case 1:
                    {
                        return a - b;
                    }
                case 2:
                    {
                        return a * b;
                    }
                case 3:
                    {
                        return a / b;
                    }
                default:
                    {
                        return a + b;
                    }
            }
        }
        static double Ur(double a, double b,double c,out double x1, out double x2)
        {
            double d = Math.Pow(b, 2) - 4 * a * c;
            x1 = 0;
            x2 = 0;
            if (d>=0)
            {
              x1 = (-b + Math.Sqrt(d)) / 2 * a;
             return x2 = (-b - Math.Sqrt(d)) / 2 * a;
            }
            else
            {
                return 0;
            }
        }
        static void Pal(int a,int b)
        {
            if ((a % 10 == b / 100)&&(a / 100 == b % 10)&&(a / 10 % 10 == b / 10 % 10))
            {
                Console.WriteLine("Палиндром");
            }
            else
            {
                Console.WriteLine("Не палиндром");
            }
        }
    }
}
