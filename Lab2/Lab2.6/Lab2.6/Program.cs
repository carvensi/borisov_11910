﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2._6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите признак фигуры");
            string prFig = Console.ReadLine();
            switch(prFig)
            {
                case "к":
                    {
                        Console.WriteLine("Радиус или диаметр?(Радиус/Диаметр)");
                        string otv = Console.ReadLine();
                        switch (otv)
                        {
                            case "Радиус":
                                {
                                    double r = Convert.ToDouble(Console.ReadLine());
                                    Console.WriteLine("S = " + (Math.PI * Math.Pow(r, 2)));
                                    Console.WriteLine("P = " + (Math.PI * r * 2));
                                    break;
                                }
                            case "Диаметр":
                                {
                                    double d = Convert.ToDouble(Console.ReadLine());
                                    Console.WriteLine("S = " + ((Math.PI * Math.Pow(d, 2))/4));
                                    Console.WriteLine("P = " + (Math.PI * d));
                                    break;
                                }
                        }
                        break;
                    }
                case "п":
                    {
                        Console.WriteLine("Введите стороны a и b");
                        double a = Convert.ToDouble(Console.ReadLine());
                        double b = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("S = " + (a*b));
                        Console.WriteLine("P = " + ((a+b)*2));
                        break;
                    }
                case "т":
                    {
                        Console.WriteLine("Введите стороны a, b, c ");
                        double a = Convert.ToDouble(Console.ReadLine());
                        double b = Convert.ToDouble(Console.ReadLine());
                        double c = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("S = " + Math.Sqrt(((a + b + c)/2)*(((a + b + c)/2)-a)* (((a + b + c)/2) - b)* (((a + b + c)/2) - c)));
                        Console.WriteLine("P = " + (a+b+c));
                        break;
                    }
            }
        }
    }
}
