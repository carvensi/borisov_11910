﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите номер масти");
            int nom = Convert.ToInt32(Console.ReadLine());
            switch(nom)
            {
                case 1:
                    {
                        Console.WriteLine("Пики");
                        break;
                    }
                case 2:
                    {
                        Console.WriteLine("Трефы");
                        break;
                    }
                case 3:
                    {
                        Console.WriteLine("Бубны");
                        break;
                    }
                case 4:
                    {
                        Console.WriteLine("Червы");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Неверно введен номер масти");
                        break;
                    }
            }
        }
    }
}
