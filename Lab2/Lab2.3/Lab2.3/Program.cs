﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите номер карты");
            int nom = Convert.ToInt32(Console.ReadLine());
            switch (nom)
            {
                case 6:
                    {
                        Console.WriteLine("Шестерка");
                        break;
                    }
                case 7:
                    {
                        Console.WriteLine("Семерка");
                        break;
                    }
                case 8:
                    {
                        Console.WriteLine("Восьмерка");
                        break;
                    }
                case 9:
                    {
                        Console.WriteLine("Девятка");
                        break;
                    }
                case 10:
                    {
                        Console.WriteLine("Десятка");
                        break;
                    }
                case 11:
                    {
                        Console.WriteLine("Валет");
                        break;
                    }
                case 12:
                    {
                        Console.WriteLine("Дама");
                        break;
                    }
                case 13:
                    {
                        Console.WriteLine("Король");
                        break;
                    }
                case 14:
                    {
                        Console.WriteLine("Туз");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Неверно введен номер карты");
                        break;
                    }
            }
        }
    }
}
