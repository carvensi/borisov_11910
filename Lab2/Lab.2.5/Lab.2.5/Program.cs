﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab._2._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("(Данные из Google)  Введите признак транспортного средства:");
            string tr = Console.ReadLine();
            switch(tr)
            {
                case "а":
                    {
                        Console.WriteLine("457,4 км/ч");
                        break;
                    }
                case "в":
                    {
                        Console.WriteLine("268,83 км/ч");
                        break;
                    }
                case "м":
                    {
                        Console.WriteLine("402 км/ч");
                        break;
                    }
                case "с":
                    {
                        Console.WriteLine("2570 км/ч");
                        break;
                    }
                case "п":
                    {
                        Console.WriteLine("574,8 км/ч");
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Неверно введен признак");
                        break;
                    }

            }
        }
    }
}
