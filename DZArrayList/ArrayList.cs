﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    /// <summary>
    /// Коллекция на основе массива
    /// </summary>
    public class ArrayList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        private T[] array;
        private int size;
        public void Add(T elem)
        {
            //Добавление в пустой список
            if(isEmpty())
            {
                array = new T[1] { elem };
                size = 1;
            }
            // Список не пустой, место есть
            if(array.Length > size)
            {
                array[size] = elem;
                size += 1;// size = size + 1;
            }
            else
            {
                var newArray = new T[array.Length * 2];
                //todo доделать 
            }
        }

        public void AddRange(T[] elems)
        {
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        public bool Contains(T elem)
        {
            bool result = false;
            if (isEmpty())
            {
                return false;
            }
            // список не пустой, ищем элемент
            for (int i = 0; i < size; i++)
            {
                if (array[i].CompareTo(elem) == 0)
                    result = true;
            }
            return result;
        }

        public int IndexOf(T elem)
        {
            int result = -1;
            if (isEmpty())
            {
                return result;
            }
            // список не пустой, ищем элемент 
            for (int i = 0; i < size; i++)
            {
                if (array[i].CompareTo(elem) == 0)
                {
                    result = i;
                    return result;
                }
            }
            return result;
        }

        public void Insert(int index, T elem)
        {
            for (int i = 0; i < size; i++)
            {
                if (i == index)
                    array[i] = elem;
            }
        }

        public bool isEmpty()
        {
            return size == 0;
        }

        public void Remove(T elem)
        {
            throw new NotImplementedException();
        }

        public void RemoveAll(T elem)
        {
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        public void Reverse()
        {
            throw new NotImplementedException();
        }

        public int Size()
        {
                return size;
        }
    }
}
