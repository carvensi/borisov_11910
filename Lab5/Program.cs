﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 5.1");
            string name, name1;
            int price, price1, col, col1, sale, sale1;
            Console.WriteLine("Введите данные о первом товаре:");
            Readt(out name, out price, out col, out sale);
            Console.WriteLine("Введите данные о втором товаре:");
            Readt(out name1, out price1, out col1, out sale1);
            Console.WriteLine("Данные о первом товаре:");
            Out( name,  price,  col,  sale);
            Console.WriteLine("Данные о втором товаре:");
            Out( name1,  price1,  col1,  sale1);
            Console.WriteLine("Общая стоймость товара: " + name + " = " + Fullpr(price, col, sale));
            Console.WriteLine("Общая стоймость товара: " + name1 + " = " + Fullpr(price1, col1, sale1));
            Console.WriteLine("Общая стоймость товара Boeing 777 = " + Fullpr(2, 1000000, 0));
            Console.WriteLine("Наименьшая стоимость товара = " + Min(price,sale,col,price1,sale1,col1, 2,0,1000000));
            Console.WriteLine("Общая стоимость товаров = " + (Fullpr(price, col, sale) + Fullpr(price1, col1, sale1) + Fullpr(2, 1000000, 0));
            Console.WriteLine("Задание 5.2");
            double a = 10.0;
            double b = 16.4;
            double c = 4.2;
            double d = 25.0;
            Console.WriteLine("При а = 10.0; b = 16.4; с = 4.2;d = 25.0:");
            Console.WriteLine("Среднее арифметическое для двух чисел: "+ Srd(a,b));
            Console.WriteLine("Среднее арифметическое для трех чисел: "+ Srd(a,b,c));
            Console.WriteLine("Среднее арифметическое для четырех чисел: " + Srd(a,b,c,d));
            Console.WriteLine("Введите количество параметров: ");
            int n = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Среднее арифмитическое для "+ n + " элементов:" + Sro(n));
            Console.WriteLine("Задание 5.3");
            Console.WriteLine("Введите число билета:");
            int bil = Convert.ToInt32(Console.ReadLine());
            Sch(bil);
            Console.WriteLine("Введите шесть цифр билета:");
            int a1 = Convert.ToInt32(Console.ReadLine());
            int a2 = Convert.ToInt32(Console.ReadLine());
            int a3 = Convert.ToInt32(Console.ReadLine());
            int a4 = Convert.ToInt32(Console.ReadLine());
            int a5 = Convert.ToInt32(Console.ReadLine());
            int a6 = Convert.ToInt32(Console.ReadLine());
            Sch(a1,a2,a3,a4,a5,a6);
            Console.WriteLine("Введите первую и вторую половину номера:");
            int b1 = Convert.ToInt32(Console.ReadLine());
            int b2 = Convert.ToInt32(Console.ReadLine());
            Sch(b1,b2);
        }
        static void Readt(out string name, out int price, out int col, out int sale)
        {
            Console.WriteLine("Введите название товара:");
            name = Console.ReadLine();
            Console.WriteLine("Введите цену товара:");
            price = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите количество товара:");
            col = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Введите скидку на товар(в процентах), если скидки нет - 0:");
            sale = Convert.ToInt32(Console.ReadLine());
        }
        static void Out(string name,int price,int col,int sale)
        {
            Console.WriteLine("Товар:"+ name);
            Console.WriteLine("Цена:" + price);
            Console.WriteLine("Количество:" + col);
            Console.WriteLine("Скидка:" + sale);
        }
        static int Fullpr(int price, int col, int sale)
        {
            return ((price - (price * sale / 100)) * col);
        }
        static int Min(int price, int sale, int col , int price1, int sale1, int col1, int price2, int sale2, int col2)
        {
            if ((Fullpr(price, col, sale) < Fullpr(price1, col1, sale1)) &&(Fullpr(price, col, sale) < Fullpr(price2, col2, sale2)))
            {
                return Fullpr(price, col, sale);
            }
            else
            {
                if ((Fullpr(price1, col1, sale1) < Fullpr(price, col, sale)) && (Fullpr(price1, col1, sale1) < Fullpr(price2, col2, sale2)))
                {
                    return Fullpr(price1, col1, sale1);
                }
                else
                {
                    return Fullpr(price2, col2, sale2);
                }
            }
        }
        static double Srd(double a,double b)
        {
            return (a + b) / 2;
        }
        static double Srd(double a, double b, double c)
        {
            return (a + b + c) / 3;
        }
        static double Srd(double a, double b,double c, double d)
        {
            return (a + b + c + d) / 4;
        }
        static double Sro(int n)
        {
            double z = 0;
            for (int i=1;i<=n;i++)
            {
                Console.WriteLine("Введите " + i + " параметр");
                double a = Convert.ToDouble(Console.ReadLine());
                z = z + a;
            }
            return z / n;
        }
        static void Sch(int a)
        {
            if ((a/100000 + a/10000%10 + a/1000%10)==(a/100%10+a/10%10+a%10))
            {
                Console.WriteLine("Счастливый билет");
            }
            else
            {
                Console.WriteLine("Не счастливый билет");
            }
        }
        static void Sch(int a1, int a2, int a3, int a4, int a5, int a6)
        {
            if ((a1+a2+a3)==(a4+a5+a6))
            {
                Console.WriteLine("Счастливый билет");
            }
            else
            {
                Console.WriteLine("Не счастливый билет");
            }
        }
        static void Sch(int a1, int a2)
        {
            if ((a1/100+a1/10%10+a1%10)==(a2 / 100 + a2 / 10 % 10 + a2 % 10))
            {
                Console.WriteLine("Счастливый билет");
            }
            else
            {
                Console.WriteLine("Не счастливый билет");
            }
        }
    }
}
