﻿using System;
using System.Reflection;

namespace KRBor
{
    public class ComeRunner
    {
        public void Run()
        {
            var Product1 = new Product("Петровых");
            var manager = new Manager();
            var ComResearcher = new ComResearcher();

            manager.Subscribe(Product1);

            ComResearcher.Subscribe(Product1);

            Product1.Come(20, 1200);

            manager.UnSubscribe(Product1);
            ComResearcher.UnSubscribe(Product1);



            
            /*
             * REFLECTION
             * Assembly asm = Assembly.LoadFrom("KRBor.dll");

            Type t = asm.GetType("KRBor.ComResearcher", true, true);
            // создаем экземпляр класса ComResearcher
            object obj = Activator.CreateInstance(t);

            // получаем метод Metod
            MethodInfo method = t.GetMethod("Metod");

            // вызываем метод, передаем ему значения для параметров и получаем результат
            object result = method.Invoke(obj, new object[] { });
            Console.WriteLine((result));
            */
        }
        public class Reflection
        {
            /* создать в классе-источнике события
            * статический метод и вызовите его,
            * непубличное поле, которому зададите значение и получите,
            * публичное свойство, которому зададите значение и получите его
            * - 1 балл
            *
            * Создайте рефлексией классы-подписчики и класс-источник,
            * вызовите событие
            * - 2 балла
            */

        }
    }
}
