﻿using System;
namespace KRBor
{
    public class ComeEventArgs
    {

        private int _Count;

        private int _Cost;



        public ComeEventArgs(int count,int cost)
        {
            _Count = count;
            _Cost = cost;
        }

        public int Count { get { return _Count; } }
        public int Cost { get { return _Cost; } }

        public bool SignUp { get; set; } = false;

    }
}
