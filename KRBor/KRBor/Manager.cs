﻿using System;
namespace KRBor
{
    public class Manager
    {
        public void Subscribe(Product product)
        {
            product.ComeEvent += SignUp;
        }

        public void UnSubscribe(Product product)
        {
            product.ComeEvent -= SignUp;
        }

        private void SignUp(object product, ComeEventArgs a)
        {
            a.SignUp = true;
            Console.WriteLine("Подпись документов для семьи " + ((Product)product).Name + " завершена");
        }
    }
}
