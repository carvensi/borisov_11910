﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace KRBor
{
    class program
    {
        public static void Main()
        {

            Console.OutputEncoding = Encoding.UTF8;
            var runner = new ComeRunner();
            runner.Run();


            Linq linq = new Linq();
            linq.Run();

            var tree = new Tree<string>();
            tree.Insert(8, "восемь");
            tree.Insert(3, "три");
            tree.Insert(1, "один");
            tree.Insert(6, "шесть");
            tree.Insert(4, "четыре");
            tree.Insert(7, "семь");
            tree.Insert(10, "десять");
            tree.Insert(9, "девять");
            tree.Insert(14, "четырнадцать");
            tree.Insert(13, "тринадцать");
            tree.Insert(16, "шестнадцать");
            tree.Insert(12, "двенадцать");
            tree.Notify += Color;
            tree.PreOrderPrint();
            tree.Notify += UnColor;
            tree.PreOrderPrint();
        }
        private static void Color()
        {
            Console.ForegroundColor = ConsoleColor.Red;
        }
        private static void UnColor()
        {
            Console.ResetColor();
        }
    }
    }
    public class Linq
    {
        public class Product
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public class Price
        {
            public int Id { get; set; }
            public int ProductId { get; set; }
            public decimal Sum { get; set; }
            public bool IsActual { get; set; }
        }

        public class Promotion
        {
            public int Id { get; set; }
            public List<PromotionalItem> items;
            public int discountPercentage { get; set; }

            public Promotion(int Id, List<PromotionalItem> items, int discountPercentage)
            {
                this.Id = Id;
                this.items = items;
                this.discountPercentage = discountPercentage;
            }

        }

        public class PromotionalItem
        {
            public int ProductId { get; set; }
            public int count;

            public PromotionalItem(int ProductId, int count)
            {
                this.ProductId = ProductId;
                this.count = count;
            }
        }

        public class Purchase
        {
            public int PriceId { get; set; }
            public int count;

            public Purchase(int PriceId, int count)
            {
                this.PriceId = PriceId;
                this.count = count;
            }
        }

        public class Score
        {
            public int Id;
            public List<Purchase> purchases;

            public Score(int Id, List<Purchase> purchases)
            {
                this.Id = Id;
                this.purchases = purchases;
            }
        }

        public void Run()
        {
            var products = new List<Product>
            {
                new Product { Id = 1, Name = "Аквариум 10 литров" },
                new Product { Id = 2, Name = "Аквариум 20 литров" },
                new Product { Id = 3, Name = "Аквариум 50 литров" },
                new Product { Id = 4, Name = "Аквариум 100 литров" },
                new Product { Id = 5, Name = "Аквариум 200 литров" },
                new Product { Id = 6, Name = "Фильтр" },
                new Product { Id = 7, Name = "Термометр" }
            };

            var prices = new List<Price>
            {
                new Price { Id = 1, ProductId = 1, Sum = 100, IsActual = false },
                new Price { Id = 2, ProductId = 1, Sum = 123, IsActual = true },
                new Price { Id = 3, ProductId = 2, Sum = 234, IsActual = true },
                new Price { Id = 4, ProductId = 3, Sum = 532, IsActual = true },
                new Price { Id = 5, ProductId = 4, Sum = 234, IsActual = true },
                new Price { Id = 6, ProductId = 5, Sum = 534, IsActual = true },
                new Price { Id = 7, ProductId = 5, Sum = 124, IsActual = false },
                new Price { Id = 8, ProductId = 6, Sum = 153, IsActual = true },
                new Price { Id = 9, ProductId = 7, Sum = 157, IsActual = true }
            };

            var scores = new List<Score>
            {
                new Score(1,new List<Purchase>{new Purchase(2,4) , new Purchase(4, 7), new Purchase(8,2)}),
                new Score(2,new List<Purchase>{new Purchase(5,2) , new Purchase(9, 1)}),
                new Score(3,new List<Purchase>{new Purchase(3,1) , new Purchase(8, 2), new Purchase(6,9)}),
                new Score(4,new List<Purchase>{new Purchase(5,5) , new Purchase(3, 1), new Purchase(9,11), new Purchase(4,6)})
            };

            //Вывод всех счетов
            foreach (Score sc in scores)
            {
                res(sc, products, prices);
            }

            var promotions = new List<Promotion>
            {
                new Promotion(1,new  List<PromotionalItem>{new PromotionalItem(5,1), new PromotionalItem(6,2) },15),
                new Promotion(2,new  List<PromotionalItem>{new PromotionalItem(4,1), new PromotionalItem(6,2) },10),
                new Promotion(3,new  List<PromotionalItem>{new PromotionalItem(3,1), new PromotionalItem(6,1) },5),
                new Promotion(4,new  List<PromotionalItem>{new PromotionalItem(2,1), new PromotionalItem(6,1) },5),
                new Promotion(5,new  List<PromotionalItem>{new PromotionalItem(1,1), new PromotionalItem(6,1) },5)
            };

            foreach (Promotion p in promotions)
            {
                WritePromotion(p, products, prices);
            }
            /*
             * 4) создать список акций (код продукта, скидка):
             * аквариум на 200 литров + 2 фильтра - скидка 15%
             * аквариум 100 литров + 2 фильтра - 10% скидка
             * любой другой аквариум + фильтр - 5% скидка
             * 5) создать список перенчень всех названий товаров в группе акции +
             * цена до скидки + цена с учетом скидки - 1 балл
             *
             * Для тех, кто выбрал вариант посложнее: написать функцию подсчета
             * суммы покупки (выявлять, есть ли в наборе продуктов акционные комплекты,
             * при их наличии делать скидку) - это + 2 балла
             */
        }

        public void res(Score score, List<Product> products, List<Price> prices)
        {
            Console.WriteLine("Счёт номер #" + score.Id);
            Console.WriteLine("{0,25}  {1,15}  {2,15}  {3,10}", "Наименование услуги", "Сумма", "Количество", "Итого");
            Console.WriteLine();
            Product prod;
            Price price;
            int res = 0;
            foreach (Purchase p in score.purchases)
            {
                price = (from k in prices
                         where k.Id == p.PriceId && k.IsActual
                         select k).First();

                prod = (from k in products
                        where k.Id == price.ProductId
                        select k).First();
                Console.WriteLine("{0,25}  {1,15}  {2,15} ", prod.Name, price.Sum, p.count);
                res += Convert.ToInt32(price.Sum * p.count);
            }
            Console.WriteLine("{0,25}  {1,15}  {2,15}  {3,10}", "", "", "", res);
            Console.WriteLine();

        }

        public void averagePrice(List<Product> products, List<Price> prices)
        {
            foreach (Product product in products)
            {
                var selected = from p in prices
                               where p.ProductId == product.Id
                               select p.Sum;
                var averagePrice = selected.Sum() / selected.Count();
                Console.WriteLine("Средняя цена продукта {0}: {1}", product.Name, averagePrice);
            }
        }

        public void WritePromotion(Promotion promotion, List<Product> products, List<Price> prices)
        {
            Console.WriteLine("Акционный комплект #" + promotion.Id);
            Console.WriteLine("{0,25}  {1,15}  {2,10}  {3,15} {4,15}", "Товар", "Количество", "Cкидка %", "Цена без скидки", "Цена со скидкой");
            Console.WriteLine();

            Product prod;
            Price price;

            foreach (PromotionalItem p in promotion.items)
            {
                prod = (from k in products
                        where k.Id == p.ProductId
                        select k).First();
                price = (from k in prices
                         where k.ProductId == p.ProductId && k.IsActual
                         select k).First();
                Console.WriteLine("{0,25}  {1,15}  {2,10}  {3,15} {4,15}", prod.Name, p.count, promotion.discountPercentage, price.Sum, (price.Sum / 100) * (100 - promotion.discountPercentage));

            }
            Console.WriteLine();

        }
    }
