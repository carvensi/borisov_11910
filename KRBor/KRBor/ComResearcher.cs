﻿using System;
using System.Reflection;

namespace KRBor
{
    public class ComResearcher
    {
        public void Subscribe(Product product)
        {
            product.ComeEvent += CheckProduct;
        }

        public void UnSubscribe(Product product)
        {
            product.ComeEvent -= CheckProduct;
        }
        static void Metod()
        {
            Console.WriteLine("Как же мне сдать экзамены, если все это очень трудно?Статический метод");
        }

        private void CheckProduct(object product, ComeEventArgs a)
        {
            Console.WriteLine("Осуществляется выплата семье, где проживает " + a.Count + " детей в размере " + a.Cost);
        }

        
    }
}
