﻿using System;
namespace KRBor
{
    public class Product
    {
        public Product(string name)
        {
            Name = name;
        }

        public string Name { get; set; }

        public event EventHandler<ComeEventArgs> ComeEvent;

        public void Come(int count, int cost)
        {
            var args = new ComeEventArgs(count,cost);

            if (ComeEvent != null)
                ComeEvent(this, args);
        }
    }
}
