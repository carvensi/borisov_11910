﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KRBor
{
    class Tree<T>
    {
        public delegate void DeepEvent();
        public event DeepEvent Notify;
        private BTreeNode<T> root;

        public void Insert(int key, T data)
        {
            if (root == null)
            {
                root = new BTreeNode<T>(key, data);
                return;
            }
            var rootCopy = root;
            bool searchPosition = true;
            while (searchPosition)
            {
                if (rootCopy.Key == key)
                {
                    rootCopy.Data = data;
                    return;
                }
                if (rootCopy.Key > key)
                {
                    if (rootCopy.Left == null)
                    {
                        rootCopy.Left = new BTreeNode<T>(key, data, rootCopy);
                        searchPosition = false;
                    }
                    else
                        rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null)
                    {
                        rootCopy.Right = new BTreeNode<T>(key, data, rootCopy);
                        searchPosition = false;
                    }
                    else
                        rootCopy = rootCopy.Right;
                }
            }
        }
        public void PreOrderPrint()
        {
            PreOrderPrintOneStep(root);
        }

        private void PreOrderPrintOneStep(BTreeNode<T> root)
        {
            Notify?.Invoke();
            if (root == null) return;
            Console.WriteLine(root.Data);
            PreOrderPrintOneStep(root.Left);
            PreOrderPrintOneStep(root.Right);
        }
    }
    public class BTreeNode<T>
    {
        public BTreeNode(int key, T data, BTreeNode<T> parent = null)
        {
            Data = data;
            Key = key;
            Parent = parent;
        }

        /// <summary>
        /// Ключ
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// Данные
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Левая ветка дерева
        /// </summary>
        public BTreeNode<T> Left { get; set; }

        /// <summary>
        /// Правая ветка дерева
        /// </summary>
        public BTreeNode<T> Right { get; set; }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        public BTreeNode<T> Parent { get; set; }
    }
}
