﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1");
            Console.WriteLine("Введите строку:");
            string str = Console.ReadLine();
            Console.WriteLine("Введите количество символов в слове:");   
            int n = Convert.ToInt32((Console.ReadLine()));
            string[] words = str.Split(new char[] { ' ', ',', '.', '!', '?', '-' });
            string[] words2 = str.Split(new char[] { ' ', ',', '.', '!', '?', '-' });
            for (int i = 0; i <= words.GetUpperBound(0); i++)
            {
                if ((words[i].Length) == n)
                {
                    Console.WriteLine(words[i]);
                }
            }

            Console.WriteLine("Задание 2");
            Console.WriteLine("Слова,начинающиеся с прописной буквы:");
            foreach (string word in words)
            {
                if (word.Length != 0)
                {
                    if (word[0] == word.ToLower()[0])
                    {
                        Console.WriteLine(word);
                    }
                }
            }
            Console.WriteLine("Задание 3");
            Console.WriteLine("Введите строку:");
            string stroka3 = Console.ReadLine();
            StringBuilder sbstr = new StringBuilder(stroka3);
            string[] arr = stroka3.Split();

            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = arr.Length - 1; j >= 0; j--)
                {
                    if (arr[i] == arr[j] && i != j)
                    {
                        sbstr.Replace(arr[i], "");
                    }
                }
            }
            Console.WriteLine(sbstr.ToString());

            Console.WriteLine("Задание 4");
            Console.WriteLine("Введите слово:");
            string slovo = Console.ReadLine();
            int k = 0;
            foreach (string word in words)
            {
                if (word == slovo)
                {
                    k++;
                }
            }
            Console.WriteLine("Заданное слово " + slovo + " встречалось " + k + " раз");

            Console.WriteLine("Задание 5");
            string Max = "";
            foreach (string word in words)
            {
                if (Max.Length < word.Length)
                {
                    Max = word;
                }
            }
            Console.WriteLine("Максимально длинное слово " + Max);

            Console.WriteLine("Задание 6");
            Console.WriteLine("Введите строку: ");
            string string6 = Console.ReadLine();
            string[] array6 = string6.Split();
            int n6 = 1;

            for (int i = 0; i < array6.Length; i++)
            {
                for (int j = array6.Length - 1; j >= 0; j--)
                {
                    if (array6[i] == array6[j] && i != j)
                    {
                        n6++;
                    }
                }
                if (n6 == 1)
                {
                    Console.WriteLine(array6[i]);
                }
                n6 = 1;
            }

            Console.WriteLine("Задание 7");
            Console.WriteLine("Введите строку: ");
            string string7 = Console.ReadLine();
            string[] array7 = string7.Split();
            Console.WriteLine("Введите число n: ");
            int n7 = Convert.ToInt32(Console.ReadLine());
            int k7 = 1;

            for (int i = 0; i < array7.Length; i++)
            {
                for (int j = array7.Length - 1; j >= 0; j--)
                {
                    if (array7[i] == array7[j] && i != j)
                    {
                        k7++;
                    }
                }
                if (k7 > n7)
                {
                    Console.WriteLine(array7[i]);
                }
                k7 = 1;
            }

            Console.WriteLine("Задание 8");
            Array.Sort(words2);
            Console.WriteLine("В алфавитном порядке");
            foreach (string word in words2)
            {
                Console.Write(word + " ");
            }
            Console.WriteLine();

            Console.WriteLine("Задание 9");
            int kol = 0;
            foreach (string word in words)
            {
                if (word.Length != 0)
                    kol++;
            }
            int l = 0;
            for (int i = 1; i < int.MaxValue; i++)
            {
                if (kol == l) break;
                for (int j = 0; j < words.Length; j++)
                {
                    if (words[j].Length == i)
                    {
                        Console.WriteLine(words[j]);
                        l++;
                    }
                }
            }
        }
    }
}