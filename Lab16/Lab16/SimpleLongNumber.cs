﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab16
{
    class SimpleLongNumber : INumber
    {
        long k { get; set; }
        public SimpleLongNumber(long k)
        { this.k = k; }
        public INumber add(INumber n)
        {
            SimpleLongNumber k1 = (SimpleLongNumber)n;
            k += k1.k;
            return this;
        }
        public INumber sub(INumber n)
        {
            SimpleLongNumber k2 = (SimpleLongNumber)n;
            try
            {
                k -= k2.k;

            }
            catch (NotNaturalNumberException)
            {
                long a;
                a = k2.k;
                k2.k = k;
                k = a;
                k -= k2.k;

            }

            return this;
        }

        public int compareTo(INumber n)
        {
            SimpleLongNumber k3 = (SimpleLongNumber)n;
            if (k > k3.k)
                return 1;
            else if (k == k3.k)
                return 0;
            else
                return -1;
        }
    }
}
