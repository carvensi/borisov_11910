﻿using System;
using System.Runtime.Serialization;

namespace Lab16
{
    [Serializable]
    internal class NotNaturalNumberException : Exception
    {
        public NotNaturalNumberException() { }
        public NotNaturalNumberException(string message) : base(message) { }
        public NotNaturalNumberException(string message, Exception ex) : base(message) { }
        protected NotNaturalNumberException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext contex) : base(info, contex) { }

    }
}