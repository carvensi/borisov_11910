﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab16
{
    class VeryLongNumber : INumber
    {
        int k { get; set; }
        long[] mas { get; set; }
        public VeryLongNumber(string sel)
        {
            string[] smas = sel.Split(' ');

            Console.WriteLine("Ведите количество");
            int p = Convert.ToInt32(Console.ReadLine());
            this.k = p;
            long[] temp = new long[k];
            for (int i = 0; i < p; i++)
            {
                temp[i] = Convert.ToInt64(smas[i]);
            }

            this.mas = temp;

        }

        public INumber add(INumber n)
        {
            VeryLongNumber g1 = (VeryLongNumber)n;

            if (k >= g1.k)
            {
                for (int i = 0; i < k; i++)
                {
                    if (i != g1.k)
                        mas[i] += g1.mas[i];
                    else
                        i = k;
                }
                return this;
            }
            else
            {
                for (int i = 0; i < g1.k; i++)
                {
                    if (i != k)
                        mas[i] += g1.mas[i];
                    else
                        i = k;

                }
                return g1;
            }


        }

        public int compareTo(INumber n)
        {

            VeryLongNumber g3 = (VeryLongNumber)n;
            if (k > g3.k)
            {
                return 1;
            }
            else
            {
                if (k == g3.k)
                    return 0;
                else
                    return -1;
            }
        }
        public INumber sub(INumber n)
        {
            VeryLongNumber g2 = (VeryLongNumber)n;

            if (k >= g2.k)
            {
                for (int i = 0; i < k; i++)
                {
                    if (i != g2.k)
                        mas[i] -= g2.mas[i];
                    else
                        i = k;
                }
                return this;
            }
            else
            {
                for (int i = 0; i < g2.k; i++)
                {
                    if (i != k)
                        mas[i] -= g2.mas[i];
                    else
                        i = k;

                }
                return g2;
            }
        }
    }
}
