﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab16
{
    interface INumber
    {

        INumber add(INumber n);
        INumber sub(INumber n);
        int compareTo(INumber n);
    }
}
