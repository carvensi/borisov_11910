﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab16
{
    class Program
    {
        static void Main(string[] args)
        {
            SimpleLongNumber a = new SimpleLongNumber(-123);
            SimpleLongNumber b = new SimpleLongNumber(34334);
            a.sub(b);
            VeryLongNumber c = new VeryLongNumber("321 22 22");
            VeryLongNumber d = new VeryLongNumber("24 24 76");
            c.add(d);

            INumber[] mas = new INumber[4];
            mas[0] = new SimpleLongNumber(-123456789);
            mas[1] = new SimpleLongNumber(123456789);
            mas[2] = new VeryLongNumber("123 234 5678 7890");
            mas[3] = new VeryLongNumber("123 234 5678 7890");
            mas[0].add(mas[1]);
            mas[2].add(mas[3]);

        }
    }
}
