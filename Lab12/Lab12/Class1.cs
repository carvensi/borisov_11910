﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab12
{
    class Vector2D
    {
        double x;

        public double X
        {
            get { return x; }
            set { x = value; }
        }
        public double y;
        public Vector2D()
        { }
        public Vector2D(double x, double y)
        {
            this.X = x;
            this.y = y;
        }
        public string toString()
        {
            return Convert.ToString("Координата x: " + x + "\n" + "Координата y: " + y);
        }
        public Vector2D add(Vector2D vector)
        {
            return new Vector2D(this.x + vector.x, this.y + vector.y);
        }
        public void add2(Vector2D vector)
        {
            this.x += vector.X;
            this.y += vector.y;
        }
        public Vector2D sub(Vector2D vector)
        {
            return new Vector2D(this.x - vector.x, this.y - vector.y);
        }
        public void sub2(Vector2D vector)
        {
            this.x -= vector.X;
            this.y -= vector.y;
        }
        public Vector2D mult(double a)
        {
            return new Vector2D(this.x * a, this.y * a);
        }
        public void mult2(double a)
        {
            this.x *= a;
            this.y *= a;
        }
        public double length()
        {
            return Math.Sqrt(this.X * this.X + this.y * this.y);
        }
        public double cos(Vector2D vector)
        {
            return (this.X * vector.x + this.y * vector.y) / (this.length() * vector.length());
        }

        public bool equals(Vector2D vector)
        {
            if (this.X == vector.X && this.y == vector.y) return true;
            return false;
        }
        public double scalarProduct(Vector2D vector)
        {
            return this.length() * vector.length() * this.cos(vector);
        }
    }
    }

