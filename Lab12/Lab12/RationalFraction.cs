﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab12
{
    class RationalFraction
    {
        public RationalFraction()
        { }
        public RationalFraction(int x, int y)
        {
            this.X = x;
            this.y = y;
        }
        int x;
        public int X
        {
            get { return x; }
            set { x = value; }

        }
        int y;
        public int Y
        {
            get { return y; }
            set { y = value; }

        }
        public void reduce()
        {

            int x = Math.Abs(this.X);
            int count = Math.Abs(this.Y);
            int min = 0;

            if (x > count) min = count;
            else min = x;

            for (int i = min; i > 0; i--)
            {
                if (x % i == 0 && count % i == 0)
                {
                    x = x / i;
                    count = count / i;
                }
            }

            this.X = x;
            this.Y = count;

        }
        public string toString()
        {
            return Convert.ToString(this.X + "/" + this.Y);
        }
        public RationalFraction add(RationalFraction rat)
        {
           RationalFraction rat2 = new RationalFraction(this.x*rat.y+this.y*rat.x,this.y * rat.y);
            rat2.reduce();
            return rat2;
            
        }
        public void add2(RationalFraction rat)
        {
            RationalFraction rat2 = new RationalFraction(this.x * rat.y + this.y * rat.x, this.y * rat.y);
            rat2.reduce();
            this.x = rat2.x;
            this.y = rat2.y;
        }
        public RationalFraction sub(RationalFraction rat)
        {
            RationalFraction rat2 = new RationalFraction((this.X * rat.y) - (this.Y * rat.x), this.Y * rat.y);
            rat2.reduce();
            return rat2;

        }
        public void sub2(RationalFraction rat)
        {
            RationalFraction rat2 = new RationalFraction(this.x * rat.y - this.y * rat.x, this.y * rat.y);
            rat2.reduce();
            this.x = rat2.x;
            this.y = rat2.y;
        }
        public RationalFraction mult(RationalFraction rat)
        {
            RationalFraction rat2 = new RationalFraction(this.x*rat.x, this.Y * rat.y);
            rat2.reduce();
            return rat2;

        }
        public void mult2(RationalFraction rat)
        {
            RationalFraction rat2 = new RationalFraction(this.x *rat.x, this.y * rat.y);
            rat2.reduce();
            this.x = rat2.x;
            this.y = rat2.y;
        }
        public RationalFraction div(RationalFraction rat)
        {
            RationalFraction rat2 = new RationalFraction(this.x * rat.y, this.y* rat.x);
            rat2.reduce();
            return rat2;

        }
        public void div2(RationalFraction rat)
        {
            RationalFraction rat2 = new RationalFraction(this.x * rat.y, this.y * rat.x);
            rat2.reduce();
            this.x = rat2.x;
            this.y = rat2.y;
        }
        public double value()
        {
            return Convert.ToDouble(this.x) / Convert.ToDouble(this.y);
        }
        public bool equals(RationalFraction rat)
        {
            rat.reduce();
            this.reduce();
            if (rat.x == this.x && rat.y == this.y)
                return true;
            return false;
        }
        public int numberPart() 
        {
            return this.x / this.y;
        }
    }
}
