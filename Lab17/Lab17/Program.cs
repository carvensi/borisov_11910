﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab17
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] Spisok = File.ReadAllLines("input.txt");
            Student[] students = new Student[Spisok.Length];
            for (int i = 0; i < Spisok.Length; i++)
            {
                string Stroka = Spisok[i];
                string[] field = Stroka.Split(';');
                Student student = new Student(field[0], Convert.ToInt32(field[1]), field[2], field[3]);
                students[i] = student;
                Console.WriteLine(students[i]);
            }
            Array.Sort(students);
            Console.WriteLine("Введите школу:");
            string Shkola = Console.ReadLine();
            string[] Saving = new string[students.Length];
            for (int i = 0; i < Spisok.Length; i++)
            {
                if (students[i].School == Shkola)
                {
                    Console.WriteLine(students[i]);
                    Student student = students[i];
                    Saving[i] = student.ToString();
                }
            }

            File.WriteAllLines("output.txt", Saving);

            Console.WriteLine("-----------------------------");

            string[] Spisok2 = File.ReadAllLines("input2.txt");
            StudentEx[] students2 = new StudentEx[Spisok2.Length];
            for (int i = 0; i < Spisok2.Length; i++)
            {
                string Stroka = Spisok2[i];
                string[] field = Stroka.Split(';');
                StudentEx student2 = new StudentEx(field[0], Convert.ToInt32(field[1]), Convert.ToInt32(field[2]), Convert.ToInt32(field[3]), Convert.ToInt32(field[4]));
                students2[i] = student2;
            }
            Array.Sort(students2);
            Console.WriteLine();
            for (int i = 0; i < Spisok2.Length; i++)
            {
                Console.WriteLine(students2[i].ToString());
            }
            string[] Saving2 = new string[students2.Length];
            for (int i = 0; i < Spisok2.Length; i++)
            {
                if (students2[i].Ex1 >= 56 && students2[i].Ex3 >= 56 && students2[i].Ex3 >= 56)
                {
                    Console.WriteLine(students2[i]);
                    StudentEx student2 = students2[i];
                    Saving2[i] = student2.ToString();
                }
                File.WriteAllLines("output2.txt", Saving2);
            }
            Console.ReadKey();
        }
    }
}
