﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab17
{
    class StudentEx : IComparable
    {
        public string Name { get; private set; }
        public int Group { get; private set; }
        public int Ex1 { get; private set; }
        public int Ex2 { get; private set; }
        public int Ex3 { get; private set; }
        public StudentEx(string name, int group, int ex1, int ex2, int ex3)
        {
            Name = name;
            Group = group;
            Ex1 = ex1;
            Ex2 = ex2;
            Ex3 = ex3;
        }
        public int CompareTo(object obj)
        {
            return Group.CompareTo(((StudentEx)obj).Group);
        }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3} {4}",
                                 Name, Group, Ex1, Ex2, Ex3);
        }
    }
}
