﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    /*
      Также как и для последовательностей, доступ к компонентам (вершинам) дерева может
      быть определен как прямой или последовательный. Будем различать понятия: позиция
      вершины дерева - однозначно идентифицирующая вершину и её положение в дереве, и
      элемент – информация, ассоциированная с вершиной дерева (в частности, просто
      хранимая вместе с ней).
      
      В основу представления бинарного дерева может быть положен принцип
      нумерации вершин. Каждая вершина дерева получает порядковый номер p(v):
      • если v является корнем, то p(v) = 1;
      • если v - левый сын вершины u, то p(v) = 2*p(u);
      • если v - правый сын вершины u, то p(v) = 2*p(u)+ l.   
         */


    /// <summary>
    /// Бинарное дерево поиска
    /// </summary>
    public class BinarySearchTree<T>
    {
        //!!!! Сигнатура методов может быть изменена, обсудим
        private BTreeNode<T> root;

        /// <summary>
        /// возвращает значение корня дерева
        /// изначально надо было вернуть позицию
        /// </summary>
        public T Root()
        {
            return root != null ? root.Data : default(T);
        }

        /// <summary>
        /// возвращает значение «родителя» для вершины в позиции p
        /// изначально надо было вернуть позицию
        /// </summary>
        public T Parent(int p)
        {
            if (p < 2) throw new ArgumentException("Элемент не имеет родителя");
            List<int> way = new List<int>(); //Путь от позиции p до корня
            while (p > 1)
            {
                way.Insert(0, p);
                p = p / 2;
            }
            var rootCopy = root;
            for (int i = 0; i < way.Count; i++)
            {
                if (way[i] % 2 == 0) //идем в левое поддерево
                {
                    if (rootCopy.Left == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Right;
                }
            }
            return rootCopy != null ? rootCopy.Parent.Data : default(T);
        }

        /// <summary>
        /// возвращает значение «самого левого сына» для вершины в позиции p.
        /// изначально надо было вернуть позицию
        /// </summary>
        public T LeftMostChild(int p)
        {
            //todo homework
            throw new NotImplementedException();
        }

        /// <summary>
        /// возвращает значение «правого брата» для вершины в позиции p.
        /// изначально надо было вернуть позицию
        /// </summary>
        public T RightSibling(int p)
        {
            //по желанию
            throw new NotImplementedException();
        }

        /// <summary>
        /// возвращает элемент дерева (хранимую информацию) для вершины в позиции p.
        /// </summary>
        public T Element(int p)
        {
            var rootCopy = root;
            var count = 0;
            while (true)
            {
                if (count == p) return rootCopy.Data;
                if (count % 2 == 0) //идем в левое поддерево
                {
                    if (rootCopy.Left == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Right;
                }
                count++;
            }
        }

        /// <summary>
        /// проверяет, является ли p позицией внутренней вершины (не листа)
        /// </summary>
        public bool IsInternal(int p)
        {
            var rootCopy = root;
            var count = 0;
            while (true)
            {
                if (count == p)
                {
                    if (rootCopy.Right != null || rootCopy.Left != null) return true;
                    else return false;
                }
                if (count % 2 == 0) //идем в левое поддерево
                {
                    if (rootCopy.Left == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Right;
                }
                count++;
            }
        }

        /// <summary>
        /// проверяет, является ли p позицией листа дерева.
        /// </summary>
        public bool IsExternal(int p)
        {
            var rootCopy = root;
            var count = 0;
            while (true)
            {
                if (count == p)
                {
                    if (rootCopy.Right != null || rootCopy.Left != null) return false;
                    else return true;
                }
                if (count % 2 == 0) //идем в левое поддерево
                {
                    if (rootCopy.Left == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Right;
                }
                count++;
            }
        }

        /// <summary>
        /// проверяет, является ли p позицией корня.
        /// </summary>
        public bool IsRoot(int p)
        {
            if (root == null) throw new ArgumentException("Дерево пустое");
            return p == 1;
        }

        /// <summary>
        /// проверяет, является ли key ключом корневого узла.
        /// </summary>
        public bool IsRootByKey(int key)
        {
            if (root == null) throw new ArgumentException("Дерево пустое");
            return root.Key == key;
        }

        /// <summary>
        /// Поиск элемента в дереве
        /// </summary>
        public bool Find(int key)
        {
            var rootCopy = root;
            if (rootCopy.Key == key)
                return true;
            while (true)
            {
                if (key > rootCopy.Key)
                {
                    if (rootCopy.Right != null)
                    {
                        rootCopy = rootCopy.Right;
                    }
                    else return false;
                }
                if (key < rootCopy.Key)
                {
                    if (rootCopy.Left != null)
                    {
                        rootCopy = rootCopy.Left;
                    }
                    else return false;
                }
            }
        }

        /// <summary>
        /// добавление в дерево значения 
        /// </summary>
        public void Insert(int key, T data)
        {
            if (root == null)
            {
                root = new BTreeNode<T>(key, data);
                return;
            }
            var rootCopy = root;
            bool searchPosition = true;
            while (searchPosition)
            {
                if (rootCopy.Key == key)
                {
                    rootCopy.Data = data;
                    return;
                }
                if (rootCopy.Key > key)
                {
                    if (rootCopy.Left == null)
                    {
                        rootCopy.Left = new BTreeNode<T>(key, data, rootCopy);
                        searchPosition = false;
                    }
                    else
                        rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null)
                    {
                        rootCopy.Right = new BTreeNode<T>(key, data, rootCopy);
                        searchPosition = false;
                    }
                    else
                        rootCopy = rootCopy.Right;
                }
            }
        }

        /// <summary>
        /// удаление узла, в котором хранится значение
        /// </summary>
        public void Remove(int key)
        {
            //доделать по желанию
            if (root == null) throw new ArgumentException("Дерево пустое");
            var rootCopy = root;
            var search = true;
            while (search)
            {
                if (rootCopy.Key > key)
                {
                    if (rootCopy.Left == null)
                        throw new ArgumentException($"Элемент с ключом {key} отсутствует");
                    rootCopy = rootCopy.Left;
                }
                else if (rootCopy.Key < key)
                {
                    if (rootCopy.Right == null)
                        throw new ArgumentException($"Элемент с ключом {key} отсутствует");
                    rootCopy = rootCopy.Right;
                }
                else //ключи совпадают 
                {
                    search = false;
                }
            }

            //с учебной точки зрения оставляю код избыточным

            //Случай 1: обоих детей нет, удаляем текущий узел и обнуляем ссылку на него у родительского узла

            if (rootCopy.Right == null && rootCopy.Left == null)
            {
                var parent = rootCopy.Parent;
                //если rootCopy - корень дерева, то просто делаем корень Null
                if (parent == null)
                    root = null;
                else
                {
                    if (parent.Right?.Key == rootCopy.Key)
                        parent.Right = null;
                    else parent.Left = null;
                }
            }

            //Случай 2: есть только 1 потомок
            if (rootCopy.Right != null && rootCopy.Left == null ||
                rootCopy.Right == null && rootCopy.Left != null)
            {
                var parent = rootCopy.Parent;
                var child = rootCopy.Right != null ? rootCopy.Right : rootCopy.Left;
                //если rootCopy - корень дерева, то ставим его потомка корнем дерева
                if (parent == null)
                {
                    child.Parent = null;
                    root = child;
                }
                else
                {
                    child.Parent = parent;
                    if (parent.Right?.Key == rootCopy.Key)
                        parent.Right = child;
                    else parent.Left = child;
                }
            }

            //Случай 3: есть оба потомка 

            if (rootCopy.Right != null && rootCopy.Left != null)
            {
                var parent = rootCopy.Parent;
                //Если левый узел m правого поддерева отсутствует (n->right->left)
                if (rootCopy.Right.Left == null)
                {
                    //Копируем из правого узла в удаляемый поля K, V и ссылку на правый узел правого потомка
                    rootCopy.Key = rootCopy.Right.Key;
                    rootCopy.Data = rootCopy.Right.Data;
                    if (rootCopy.Right.Right != null)
                        rootCopy.Right.Right.Parent = rootCopy;
                    rootCopy.Right = rootCopy.Right.Right;
                }
                else
                {
                    //Возьмём самый левый узел m, правого поддерева n->right;
                    var mostLeftChild = rootCopy.Right.Left;
                    while (mostLeftChild.Left != null)
                        mostLeftChild = mostLeftChild.Left;
                    //Скопируем данные (кроме ссылок на дочерние элементы) из m в n;
                    rootCopy.Key = mostLeftChild.Key;
                    rootCopy.Data = mostLeftChild.Data;
                    //удалим узел m (изначально предлагается сделать это рекурсивно, но я сделаю без рекурсии)
                    if (mostLeftChild.Right == null)
                    {
                        mostLeftChild.Parent.Left = null;
                    }
                    else
                    {
                        mostLeftChild.Right.Parent = mostLeftChild.Parent;
                        mostLeftChild.Parent.Left = mostLeftChild.Right;
                    }
                }
            }

        }

        //https://learnc.info/adt/binary_tree_traversal.html вывод деревьев

        /// <summary>
        /// Вывод в глубину прямой
        /// Прямой (pre-order)        
        /// Посетить корень    
        /// Обойти левое поддерево    
        /// Обойти правое поддерево
        /// </summary>
        public void PreOrderPrint()
        {
            PreOrderPrintOneStep(root);
        }

        private void PreOrderPrintOneStep(BTreeNode<T> root)
        {
            if (root == null) return;
            Console.WriteLine(root.Data);
            PreOrderPrintOneStep(root.Left);
            PreOrderPrintOneStep(root.Right);
        }

        /// <summary>
        /// Вывод в глубину Симметричный или поперечный (in-order)
        /// Обойти левое поддерево
        /// Посетить корень
        /// Обойти правое поддерево
        /// </summary>
        public void InOrderPrint()
        {
            if (root == null) throw new ArgumentException();
            InOrderPrint(root);
        }

        public void InOrderPrint(BTreeNode<T> node)
        {
            if (node.Left != null)
            {
                InOrderPrint(node.Left);
            }
            Console.Write(node.Data.ToString() + "  ");
            if (node.Right != null)
            {
                InOrderPrint(node.Right);
            }
        }

        /// <summary>
        /// Вывод в глубину В обратном порядке (post-order)
        /// Обойти левое поддерево
        /// Обойти правое поддерево
        /// Посетить корень
        /// </summary>
        public void PostOrderPrint()
        {
            PostOrderPrintOneStep(root);
        }

        private void PostOrderPrintOneStep(BTreeNode<T> node)
        {
            if (node == null) return;
            PostOrderPrintOneStep(node.Left);
            PostOrderPrintOneStep(node.Right);
            Console.WriteLine(node.Data);
        }

        /// <summary>
        /// Вывод в ширину
        /// </summary>
        public void PrintDepth()
        {
            PrintLevel(new List<BTreeNode<T>>() { root });
        }

        /// <summary>
        /// Вывод одного уровня дерева
        /// </summary>
        /// <param name="list"></param>
        private void PrintLevel(List<BTreeNode<T>> list)
        {
            if (list == null || list.Count == 0)
                return;
            var nextLevel = new List<BTreeNode<T>>();
            foreach (var node in list)
            {
                if (node != null)
                {
                    Console.Write(node.Data + "  ");
                    if (node.Left != null)
                        nextLevel.Add(node.Left);
                    if (node.Right != null)
                        nextLevel.Add(node.Right);
                }
            }
            Console.WriteLine();
            PrintLevel(nextLevel);
        }

        /// <summary>
        /// Сбалансировать дерево *
        /// </summary>
        public void Balance()
        {
        }

        public void BigRotateLeft()
        {
            root = BigRotateLeft(root);
            root = BigRotateRight(root);
            Console.WriteLine("После поворота");
            PrintDepth();
        }

        /// <summary>
        /// Большой левый поворот
        /// </summary>
        private BTreeNode<T> BigRotateLeft(BTreeNode<T> oldSubTreeRoot)
        {
            oldSubTreeRoot.Right = RotateRight(oldSubTreeRoot.Right);
            var newSubTreeRoot = RotateLeft(oldSubTreeRoot);
            return newSubTreeRoot;
        }
        /// <summary>
        /// Большой правый поворот
        /// </summary>
        private BTreeNode<T> BigRotateRight(BTreeNode<T> oldSubTreeRoot)
        {
            oldSubTreeRoot.Left = RotateLeft(oldSubTreeRoot.Left);
            var newSubTreeRoot = RotateRight(oldSubTreeRoot);
            return newSubTreeRoot;
        }
        /// <summary>
        /// Малый правый поворот
        /// </summary>
        private BTreeNode<T> RotateRight(BTreeNode<T> oldSubTreeRoot)
        {
            var newSubTreeRoot = oldSubTreeRoot.Left;
            newSubTreeRoot.Parent = oldSubTreeRoot.Parent;
            oldSubTreeRoot.Left = newSubTreeRoot.Right;
            oldSubTreeRoot.Left.Parent = oldSubTreeRoot;
            newSubTreeRoot.Right = oldSubTreeRoot;
            oldSubTreeRoot.Parent = newSubTreeRoot;
            return newSubTreeRoot;
        }

        /// <summary>
        /// Малый левый поворот
        /// </summary>
        private BTreeNode<T> RotateLeft(BTreeNode<T> oldSubTreeRoot)
        {
            var newSubTreeRoot = oldSubTreeRoot.Right;
            newSubTreeRoot.Parent = oldSubTreeRoot.Parent;
            oldSubTreeRoot.Right = newSubTreeRoot.Left;
            newSubTreeRoot.Left.Parent = oldSubTreeRoot;
            newSubTreeRoot.Left = oldSubTreeRoot;
            oldSubTreeRoot.Parent = newSubTreeRoot;
            return newSubTreeRoot;
        }

    }
}
