﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deq
{
    public class DeqOnMassive<T>
    {

        private T[] elements;
        private int cursorFirst;
        private int cursorLast;

        public DeqOnMassive()
        {

            elements = new T[8];
            cursorLast = elements.Length/2 ;
            cursorFirst = cursorLast-1;

        }


        /// <summary>
        /// Добавление в конец
        /// </summary>
        public void PushBack(T item)
        {   
            elements[cursorLast] = item;
            cursorLast++;
            if (cursorLast == elements.Length)
            {
                T[] second = new T[elements.Length*2];

                Array.Copy(elements, cursorFirst + 1, second, cursorFirst + elements.Length / 2+1, Size());
                cursorFirst += elements.Length / 2;
                cursorLast += elements.Length / 2;
                elements = second;
            }
        }

        /// <summary>
        /// Удаление с конца
        /// </summary>
        public T PopBack()
        {
            if (Size() == 0) throw new IndexOutOfRangeException();
            cursorLast--;
            return (elements[cursorLast]);
        }

        /// <summary>
        /// Добавление в начало
        /// </summary>
        public void PushFront(T item)
        {
            elements[cursorFirst] = item;
            cursorFirst--;
            if (cursorFirst == -1)
            {
                T[] second = new T[elements.Length * 2];
                Array.Copy(elements, cursorFirst + 1, second, cursorFirst + elements.Length / 2+1, Size());             
                cursorFirst += elements.Length / 2;
                cursorLast += elements.Length / 2;
                elements = second;
            }
        }

        /// <summary>
        /// Удаление с начала
        /// </summary>
        public T PopFront()
        {
            if (Size()==0) throw new IndexOutOfRangeException();
            cursorFirst++;
            return (elements[cursorFirst]);
        }

        /// <summary>
        /// Проверка на пустоту
        /// </summary>
        public bool IsEmpty()
        {
            return cursorLast -cursorFirst==1;
        }

        /// <summary>
        /// Размер коллекции
        /// </summary>
        public int Size()
        {
            return cursorLast - cursorFirst-1;
        }

    }
}
