﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Deq
{
    class Class1
    {
        public static void Main(String[] args)
        {
            DeqOnMassive<int> deq = new DeqOnMassive<int>();
            deq.PushBack(5);
            deq.PushBack(5);
            deq.PushBack(5);
            deq.PushBack(5);
            deq.PushFront(4);
            deq.PushFront(4);
            deq.PushFront(4);
            deq.PushFront(4);
            deq.PushFront(4);
            deq.PushFront(4);
            deq.PushFront(4);
            deq.PushFront(4);
            deq.PushBack(5);
            deq.PushBack(5);
            deq.PushBack(5);
            deq.PushBack(5);
            for (int i = 0; i < 16; i++) Console.WriteLine(deq.PopBack());
            Console.ReadKey();
        }
    }
}
