﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Hyperbola : Function
    {
        public readonly double A;

        public Hyperbola(double a)
        {
            A = a;
            style = "Гипербола";
        }

        public override double Count(double x)
        {
            return A / x;
        }
    }
}
