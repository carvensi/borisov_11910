﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Rectangle : Figure 
    {
        string style;
        public Rectangle() 
        {
            style = "null";
        }

        public Rectangle(double x) : base(x, "прямоугольник")
        {
            style = "Квадрат";
        }
        public Rectangle(double x, double y) : base(x, y, "прямоугольник")
        {
            style = "";
        }
        public override double Area()
        {
            return Width * Height;
        }
        public override double Perimeter()
        {
            return (Width + Height) * 2;
        }
        public override string ShowStyle()
        {
            return style;
        }
    }
}
