﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Internet : Edition
    {
        public string Link;
        public string Annotation;
        public Internet(string name,string lastname,string link,string annot):base(name,lastname)
        {
            this.Link = link;
            this.Annotation = annot;
        }
        public override string getinfo()
        {
            return ("\n" + "Название " + Name +
                "\n" + "Фамилия автора " + Lastname +
                "\n" + "Ссылка " + this.Link +
                "\n" + "Аннотация " + this.Annotation);
        }
    }
}
