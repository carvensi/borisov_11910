﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Line : Function
    {
        public double A;
        public double B;

        public Line(double a, double b)
        {
            A = a;
            B = b;
            style = "Линия";
        }

        public override double Count(double x)
        {
            return A * x + B;
        }
    }
}
