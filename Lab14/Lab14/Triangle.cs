﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Triangle : Figure 
    {
        string style;
        public Triangle()
        {
            style = "null";
        }
        public Triangle(double x) : base(x, "треугольник")
        {
            style = "Равносторонний";
        }

        public Triangle(double x, double y) : base(x, y, "треугольник")
        {
            style = "Равнобедренный";
        }
        public Triangle(double x, double y, double z) : base(x, y, z, "треугольник")
        {
            style = "";
        }

        public override double Area()
        {
            if (style == "Равносторонний")
            {
                return (Math.Sqrt(3) * Width * Width) / 4;
            }
            if (style == "Равнобедренный")
            {
                return (Height / 4) * Math.Sqrt(4 * Width * Width - Height * Height);
            }
            else
            {
                double p = (Width + Height + Length) / 2;
                return Math.Sqrt(p * (p - Width) * (p - Height) * (p - Length)); 
            }
        }

        public override double Perimeter()
        {
            return Width + Height + Length;
        }

        public override string ShowStyle()
        {
            return style;
        }
    }
}
