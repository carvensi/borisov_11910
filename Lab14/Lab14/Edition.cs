﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    abstract class Edition
    {
        public string Name;
        public string Lastname;
        public Edition(string name, string lastname)
        {
            Name = name;
            Lastname = lastname;
        }
        public abstract string getinfo();
    }
}
