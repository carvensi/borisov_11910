﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Article : Edition
    {
        public string Nameofmag;
        public int Num;
        int Year { get; set; }
        public Article(string name,string lastname,string namemag,int num,int year):base(name,lastname)
        {   
            this.Nameofmag = namemag;
            this.Num = num;
            this.Year = year;
        }
        public override string getinfo()
        {
            return ("\n" + "Название " + Name +
                "\n" + "Фамилия автора " + Lastname +
                "\n" + "Название журнала " + this.Nameofmag +
                "\n" + "Номер " + this.Num + 
                "\n" + "Год издания " + this.Year);
        }
    }
}
