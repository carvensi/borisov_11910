﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Book : Edition
    {
        int Year { get; set; }
        string Publisher { get; set; }
        public Book(string name, string lastname, int year, string publ):base(name,lastname)
        {
            this.Year = year;
            this.Publisher = publ;
        }

        public override string getinfo()
        {
            return ("\n" + "Название книги " + Name + 
                "\n" + "Фамилия автора " + Lastname + 
                "\n" + "Год издания " + this.Year + 
                "\n" + "Издательство "+ this.Publisher);
        }

    }
}
