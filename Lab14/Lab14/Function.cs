﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    abstract class Function
    {
        public string style;
        public abstract double Count(double x);
        public string getStyle()
        {
            return (style);
        }
    }
}
