﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Circle : Figure 
    {
        string style;
        public Circle() { style = "null"; } 

        public Circle(double x) : base(x, "Круг") { style = ""; }

        public override double Area()
        {
            return Math.PI * Width * Width;
        }

        public override double Perimeter()
        {
            return 2 * Math.PI * Width;
        }

        public override string ShowStyle()
        {
            return style;
        }

    }
}
