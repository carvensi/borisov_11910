﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Program
    {
        static void Main(string[] args)
        {
            Figure[] Фигуры = new Figure[6];
            Фигуры[0] = new Triangle(3.0);
            Фигуры[1] = new Triangle(4.0, 5.0);
            Фигуры[2] = new Triangle(3.0, 4.0, 5.0);
            Фигуры[3] = new Rectangle(3.0);
            Фигуры[4] = new Rectangle(3.0, 4.0);
            Фигуры[5] = new Circle(3.0);

            for (int i = 0; i < Фигуры.Length; i++)
            {
                Console.WriteLine("Фигура - " + Фигуры[i].name + " " + Фигуры[i].ShowStyle());
                Console.WriteLine("Площадь равна: " + Фигуры[i].Area());
                Console.WriteLine("Периметр равен: " + Фигуры[i].Perimeter());
                Console.WriteLine();
            }

            Function[] func = new Function[3];
            func[0] = new Line(2, 3);
            func[1] = new Kub(2, 3, 4);
            func[2] = new Hyperbola(3);
            double x = 3.6;
            foreach (Function f in func)
                Console.WriteLine("Значение функции {0} для x = {1} равно {2}", f.getStyle(), x, f.Count(x));

            Book book1 = new Book("Колобок","Пушкин",1998,"Россия");
            Article art = new Article("ФБР", "Путин", "Россиюшка", 25, 2019);
            Internet inter = new Internet("Что не так с Россией?","Зайцев","vk.com","21+");
            Console.WriteLine(book1.getinfo());
            Console.WriteLine(art.getinfo());
            Console.WriteLine(inter.getinfo());
            Edition[] edit = new Edition[3];
            edit[0] = new Book("Колобок", "Пушкин", 1998, "Россия");
            edit[1] = new Article("ФБР", "Путин", "Россиюшка", 25, 2019);
            edit[2] = new Internet("Что не так с Россией?", "Зайцев", "vk.com", "21+");
            for(int i = 0;i<edit.Length;i++)
            {
                Console.WriteLine(edit[i].getinfo());
            }
            Console.WriteLine("\n");
            Console.WriteLine("Введите фамилию автора");
            string Auth = Console.ReadLine();
            for (int i = 0; i < edit.Length; i++)
            {
                if (edit[i].Lastname == Auth)
                    Console.WriteLine(edit[i].getinfo());
            }

        }
    }

}

