﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Kub : Function
    {
        public readonly double A;
        public readonly double B;
        public readonly double C;

        public Kub(double a, double b, double c)
        {
            A = a;
            B = b;
            C = c;
            style = "Куб";
        }

        public override double Count(double x)
        {
            return A * x * x + B * x + C;
        }
    }
}
