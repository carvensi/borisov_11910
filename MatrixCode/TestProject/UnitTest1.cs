using System.Collections.Generic;
using NUnit.Framework;

namespace TestProject
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void TestDecode()
        {
            int[][] array =
            {
                new[] {1, 2, 3},
                new[] {4, 5, 6},
                new[] {7, 8, 9},
            };
            MatrixCode.MatrixCode matrix = new MatrixCode.MatrixCode(array);
            Assert.AreEqual(array, matrix.Decode());
        }

        [Test]
        public void TestInsert()
        {
            MatrixCode.MatrixCode matrix = new MatrixCode.MatrixCode(new[]
            {
                new[] {1, 2, 3},
                new[] {4, 0, 6},
                new[] {7, 8, 9},
            });
            matrix.Insert(1, 1, 5);
            Assert.AreEqual(new[]
            {
                new[] {1, 2, 3},
                new[] {4, 5, 6},
                new[] {7, 8, 9},
            }, matrix.Decode());
            matrix.Insert(1, 1, 10);
            Assert.AreEqual(new[]
            {
                new[] {1, 2, 3},
                new[] {4, 10, 6},
                new[] {7, 8, 9},
            }, matrix.Decode());
        }

        [Test]
        public void TestDelete()
        {
            MatrixCode.MatrixCode matrix = new MatrixCode.MatrixCode(new[]
            {
                new[] {1, 2, 3},
                new[] {4, 5, 6},
                new[] {7, 8, 9},
            });
            matrix.Delete(1, 1);
            Assert.AreEqual(new[]
            {
                new[] {1, 2, 3},
                new[] {4, 0, 6},
                new[] {7, 8, 9},
            }, matrix.Decode());
        }

        [Test]
        public void TestMinList()
        {
            MatrixCode.MatrixCode matrix = new MatrixCode.MatrixCode(new[]
            {
                new[] {1, 1, 2},
                new[] {2, 0, 1},
                new[] {3, 1, 3},
            });
            List<int> minList = matrix.MinList();
            Assert.AreEqual(new List<int> {1, 0, 1}, minList);
        }

        [Test]
        public void TestSumDiag()
        {
            MatrixCode.MatrixCode matrix = new MatrixCode.MatrixCode(new[]
            {
                new[] {1, 1, 1},
                new[] {1, 1, 1},
                new[] {1, 1, 1},
            });
            Assert.AreEqual(5, matrix.SumDiag());
        }

        [Test]
        public void TestTransp()
        {
            MatrixCode.MatrixCode matrix = new MatrixCode.MatrixCode(new[]
            {
                new[] {3, 0, 0},
                new[] {2, 3, 0},
                new[] {1, 2, 3},
            });
            matrix.Transp();
            Assert.AreEqual(new[]
            {
                new[] {3, 2, 1},
                new[] {0, 3, 2},
                new[] {0, 0, 3},
            }, matrix.Decode());
        }

        [Test]
        public void TestSumCols()
        {
            MatrixCode.MatrixCode matrix = new MatrixCode.MatrixCode(new[]
            {
                new[] {1, 1, 0},
                new[] {0, 1, 0},
                new[] {1, 0, 0},
            });
            matrix.SumCols(0, 1);
            Assert.AreEqual(new[]
            {
                new[] {2, 1, 0},
                new[] {1, 1, 0},
                new[] {1, 0, 0},
            }, matrix.Decode());
        }
    }
}