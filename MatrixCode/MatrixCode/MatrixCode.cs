﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MatrixCode
{
    public class MatrixCode
    {
        private class Element
        {
            public int Column { get; set; }
            public int Row { get; set; }
            public int Value { get; set; }

            public Element(int column, int row, int value)
            {
                Column = column;
                Row = row;
                Value = value;
            }

            public Element Next { get; set; }
        }

        public const int Size = 3;

        private Element _head;

        public MatrixCode(int[][] matrix)
        {
            if (matrix.Length != Size || matrix[0].Length != Size)
            {
                throw new ArgumentException("");
            }

            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (matrix[i][j] != 0)
                    {
                        Insert(j, i, matrix[i][j]);
                    }
                }
            }
        }

        public int[][] Decode()
        {
            int[][] matrix = new int[Size][];
            for (int i = 0; i < Size; i++)
            {
                matrix[i] = new int [Size];
            }

            Element cur = _head;
            while (cur != null)
            {
                matrix[cur.Row][cur.Column] = cur.Value;
                cur = cur.Next;
            }

            return matrix;
        }

        public void Insert(int i, int j, int value)
        {
            if (_head == null)
            {
                _head = new Element(i, j, value);
            }
            else
            {
                Element prev = null;
                Element cur = _head;
                bool exists = false;
                while (cur != null)
                {
                    if (cur.Column == i && cur.Row == j)
                    {
                        var newEl = new Element(i, j, value);
                        newEl.Next = cur.Next;
                        if (prev != null) prev.Next = newEl;
                        exists = true;
                        break;
                    }

                    prev = cur;
                    cur = cur.Next;
                }

                if (!exists)
                {
                    // head != null и соотв. prev != null
                    // ReSharper disable once PossibleNullReferenceException
                    prev.Next = new Element(i, j, value);
                }
            }
        }

        public void Delete(int i, int j)
        {
            if (_head.Column == i && _head.Row == j)
            {
                _head = _head.Next;
            }
            else
            {
                Element cur = _head;
                while (cur.Next != null)
                {
                    if (cur.Next.Column == i && cur.Next.Row == j)
                    {
                        cur.Next = cur.Next.Next;
                        break;
                    }

                    cur = cur.Next;
                }
            }
        }

        public List<int> MinList()
        {
            List<int> minList = new List<int>(Size);
            for (int i = 0; i < Size; i++)
            {
                minList.Add(int.MaxValue);
            }

            int[] columnCounter = new int[Size];

            Element cur = _head;
            while (cur != null)
            {
                if (cur.Value < minList[cur.Column])
                {
                    minList[cur.Column] = cur.Value;
                }

                columnCounter[cur.Column]++;
                cur = cur.Next;
            }

            for (int i = 0; i < Size; i++)
            {
                if (columnCounter[i] != Size && 0 < minList[i])
                {
                    minList[i] = 0;
                }
            }

            return minList;
        }

        public int SumDiag()
        {
            int sum = 0;
            Element cur = _head;
            while (cur != null)
            {
                if (cur.Column == cur.Row || cur.Column == Size - cur.Row - 1)
                {
                    sum += cur.Value;
                }

                cur = cur.Next;
            }

            return sum;
        }

        public void Transp()
        {
            Element cur = _head;
            while (cur != null)
            {
                int col = cur.Column;
                cur.Column = cur.Row;
                cur.Row = col;

                cur = cur.Next;
            }
        }

        public void SumCols(int j1, int j2)
        {
            int[] secondTerms = new int[Size];
            bool[] firstExists = new bool[Size];

            Element cur = _head;
            while (cur != null)
            {
                if (cur.Column == j2)
                {
                    secondTerms[cur.Row] = cur.Value;
                }

                cur = cur.Next;
            }

            Element prev = null;
            cur = _head;
            while (cur != null)
            {
                if (cur.Column == j1)
                {
                    cur.Value += secondTerms[cur.Row];
                    firstExists[cur.Row] = true;
                }

                prev = cur;
                cur = cur.Next;
            }

            for (int i = 0; i < Size; i++)
            {
                if (!firstExists[i] && secondTerms[i] != 0)
                {
                    //secondTerms[i] != 0 исключает то, что head == null и prev == null
                    // ReSharper disable once PossibleNullReferenceException
                    prev.Next = new Element(j1, i, secondTerms[i]);
                    prev = prev.Next;
                }
            }
        }
    }
}