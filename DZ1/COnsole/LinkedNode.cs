﻿using System;
namespace COnsole
{
    public class LinkedNode<T>
    {
        
            
            public T Data { get; set; }
            /// <summary>
            /// Ссылка на предыдущий элемент
            /// </summary>
            public LinkedNode<T> PrevNode { get; set; }
            /// <summary>
            /// Ссылка на след элемент
            /// </summary>
            public LinkedNode<T> NextNode { get; set; }
        
    }
}
