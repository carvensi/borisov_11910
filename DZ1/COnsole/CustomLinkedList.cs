﻿using System;
using System.Text;
using CustomCollection;

namespace COnsole
{
    public class CustomLinkedList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        private LinkedNode<T> head;

        ///<inheritdoc/>
        public void Add(T elem)
        {
            if (head == null)
                head = new LinkedNode<T>() { Data = elem };
            else
            {
                var node = head;
                while (node.NextNode != null)
                {
                    node = node.NextNode;
                }
                var newNode = new LinkedNode<T>() { Data = elem };
                node.NextNode = newNode;
                newNode.PrevNode = node;
            }
        }

        ///<inheritdoc/>
        public void AddRange(T[] elems)
        {
            if (head == null)
            {
                foreach (T elem in elems)
                {
                    head = new LinkedNode<T> { Data = elem };
                }
            }
            else
            {
                var node = head;
                foreach (T elem in elems)
                {
                    while (node.NextNode != null)
                        node = node.NextNode;
                    var newNode = new LinkedNode<T> { Data = elem };
                    node.NextNode = newNode;
                    newNode.PrevNode = node;

                }
            }
        }

        ///<inheritdoc/>
        public void Clear()
        {
            head = null;
        }

        ///<inheritdoc/>
        public bool Contains(T elem)
        {
            if (isEmpty()) return false;
            var node = head;

            while (node.NextNode != null)
            {
                if (node.Data.CompareTo(elem) != 0)
                    node = node.NextNode;
                else return true;


            }
            return false;

        }

        ///<inheritdoc/>
        public int IndexOf(T elem)
        {
            
            var node = head;
            int position = 1;
            while(node.NextNode!=null)
            {
                if (node.Data.CompareTo(elem) == 0)
                    return position;
                node = node.NextNode;
                position += 1;
            }
            return 0;
        }

        ///<inheritdoc/>
        public void Insert(int index, T elem)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public bool isEmpty()
        {
            return head == null;
        }

        ///<inheritdoc/>
        public void Remove(T elem)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void RemoveAll(T elem)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Reverse()
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public int Size()
        {
            if (isEmpty()) return 0;
            var node = head;
            int size = 0;
            while(node.NextNode!=null)
            {
                size += 1;
                node = node.NextNode;
            }
            return size;
        }

        /// <summary>
        /// Вывести список строкой
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (head == null) return null;
            var node = head;
            var sb = new StringBuilder();
            while (node != null)
            {
                sb.Append(" " + node.Data.ToString());
                node = node.NextNode;
            }
            return sb.ToString();
        }
    
        
    }
}
