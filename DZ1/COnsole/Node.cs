﻿using System;
namespace CustomCollection
{
    /// <summary>
    /// Элемент списка
    /// </summary>
    public class Node<T> : IComparable<T>
    {
        /// <summary>
        /// Информационное поле
        /// </summary>
       public T Data { get; set; }

       /// <summary>
       /// Ссылка на следующий элемент
       /// </summary>
        public Node<T> NextNode { get; set; }

      
        

        public int CompareTo(T other)
        {
            throw new NotImplementedException();
        }
    }
}
