﻿using System;
using CustomCollection;
namespace COnsole
{
    public class RunnerArray
    {
        public void Run()
        {
            var students = new ArrayList<Student>
            {

               new Student
                {
                    City = "Казань",
                    Fio = "Гарифуллина Рената Наилевна",
                    Birthday = new DateTime(1800, 5, 5),
                    Mark = 4.38953F
                }
            };

            ///Add Check
            students.Add(new Student
            {
                City = "Казань",
                Fio = "Абрамский Михаил Михайлович",
                Birthday = new DateTime(1800, 5, 5),
                Mark = 5F
            });

            students.Add(new Student
            {
                City = "Kazan",
                Fio = "La",
                Birthday = new DateTime(1700, 4, 4),
                Mark = 3F

            });

            ///Insert Check
               students.Insert(1,
                     new Student
               {
                   City = "Kazan",
                   Fio = "d",
                   Birthday = new DateTime(1400, 3, 4),
                   Mark = 5F
               }) ;

            ///IndexOf check
            Console.WriteLine("Индекс " + students.IndexOf(new Student
            {
                City = "Kazan",
                Fio = "La",
                Birthday = new DateTime(1700, 4, 4),
                Mark = 3F

            }));

            ///Size Check
            Console.WriteLine("Размер " + students.Size());

            ///Contain Check
            switch
                (
                students.Contains
                   (
                    new Student
            {
                City = "Казань",
                Fio = "Гарифуллина Рената Наилевна",
                Birthday = new DateTime(1800, 5, 5),
                Mark = 4.38953F
            }
                    )
                )
            {
                case true:
                    {
                        Console.WriteLine("Содержит");
                        break;
                    }
                case false:
                    {
                        Console.WriteLine("Не содержит");
                        break;
                    }
            }

            foreach (var s in students)
            {
                Console.WriteLine(s.ToString());
            }
        }
    }
}
