﻿using System;
namespace COnsole
{
    public class RunnerLinked
    {
       public void Run()
        {
            var list = new CustomLinkedList<int>();
            
            list.Add(5);
            list.Add(7);
            list.Add(9);
            list.Add(5);
            list.Add(13);
            list.Add(66);
            list.Add(5);
            list.AddRange(new[] { 4, 2, 5, 6, 15, 3, 2 });

            Console.WriteLine(list.Contains(5));
            Console.WriteLine(list.IndexOf(5));
            Console.WriteLine(list.Size());
           

            Console.WriteLine(list.ToString());
            list.Clear();
            Console.WriteLine(list.isEmpty());
        }
    }
}
