﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CustomCollection
{
    public class CustomList<T> : ICustomCollection<T>, IEnumerable<T> where T: IComparable<T>
    {
        /// <summary>
        /// 
        /// </summary>
        private Node<T> head;


        /// <inheritdoc/>
        public void Add(T elem)
        {
            if (head == null)
                head = new Node<T>() { Data = elem };

            else
            {
                var node = head;
                while (node.NextNode != null)
                    node = node.NextNode;
                node.NextNode = new Node<T>() { Data = elem };
            }
        }
        /// <inheritdoc/>
        public void AddRange(T[] elems)
        {
            if(head == null)
            {
                foreach(T elem in elems)
                {
                    head = new Node<T> { Data = elem };
                }
            }
            else
            {
                var node = head;
                foreach(T elem in elems)
                {
                    while (node.NextNode != null)
                        node = node.NextNode;
                    node.NextNode = new Node<T> { Data = elem };
                }
            }
        }
        /// <inheritdoc/>
        public void Clear()
        {
            head = null;
          
        }
        /// <inheritdoc/>
        public bool Contains(T elem)
        {
           
            throw new NotImplementedException();
        }
        /// <inheritdoc/>
        public int IndexOf(T elem)
        {
            throw new NotImplementedException();
        }
        /// <inheritdoc/>
        public void Insert(int index, T item)
        {
            throw new NotImplementedException();
        }
        /// <inheritdoc/>
        public bool isEmpty()
        {
            return head == null;
        }
        /// <inheritdoc/>
        public void Remove(T elems)
        {
            var node = head;
            if (isEmpty()) return;
            Node<T> prevNode = null;
            while(node!=null)
            {
                if(node.Data.CompareTo(elems)!=0)
                {
                    prevNode = node;
                    node = node.NextNode;
                }
                else
                {
                    if(prevNode == null)
                    {
                        head = head.NextNode;
                        node = head.NextNode;
                        break;
                    }
                    else
                    {
                        prevNode.NextNode = node.NextNode;
                        node = node.NextNode;
                        break;
                    }
                }
            }
            

        }
        /// <inheritdoc/>
        public void RemoveAll(T elem)
        {
            if (isEmpty()) return;
            var node = head;
            Node<T> prevNode = null;
            while(node!=null)
            {
                if(node.Data.CompareTo(elem) != 0)
                {
                    prevNode = node;
                    node = node.NextNode;
                }
                else
                {
                    if (prevNode == null) //головной элемент
                    {
                        head = head.NextNode;
                        node = head.NextNode;
                    }

                    else
                    {
                        prevNode.NextNode = node.NextNode;
                        node = node.NextNode;
                    }
                }
            }
        }
        /// <inheritdoc/>
        public void RemoveAt(int index)
        {
            var node = head;
            Node<T> prevNode = null;
            if (isEmpty()) return;
            int Ind = 0;
            while(node!=null)
            {
                if(Ind != index)
                {
                    prevNode = node;
                    node = node.NextNode;
                    Ind++;
                }
                else
                {
                    if(prevNode == null)
                    {
                        head = head.NextNode;
                        node = head.NextNode;
                        break;
                    }
                    else
                    {
                        prevNode.NextNode = node.NextNode;
                        node = node.NextNode;
                        break;
                    }
                }
            }
        }
        /// <inheritdoc/>
        public void Reverse()
        {
            Node<T> node;
            if (isEmpty()) return;
            node = new Node<T> { Data = head.Data };
            while(head != null)
            {
                head = head.NextNode;
                if (isEmpty())
                {
                    head = node;
                    return;
                }
                node = new Node<T>() { Data = head.Data, NextNode = node };
            }

        }
        /// <inheritdoc/>
        public int Size()
        {
            var node = head;
            Node<T> prevNode = null;
            int size = 0;
            if (isEmpty()) return 0;
            while(node!=null)
            {
                prevNode = node;
                node = node.NextNode;
                size++;
            }
            return size;
        }

        public override string ToString()
        {
            if (head == null) return "null";
            var node = head;
            var sb = new StringBuilder();
            while(node != null)
            {
                sb.Append(" " + node.Data.ToString());
                node = node.NextNode;
            }
            return sb.ToString();
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new ListEnumerator<T>(head);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
    /// <summary>
    /// Перечислитель для односвязного линейного списка
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ListEnumerator<T> : IEnumerator<T>
    {
        /// <summary>
        /// Головной элемент списка
        /// </summary>
        private Node<T> _head;

        /// <summary>
        /// Переменная, чтобы идти по списку
        /// </summary>
        private Node<T> node;

        public ListEnumerator(Node<T> head)
        {
            _head = head;
            node = new Node<T> { Data = default(T), NextNode = head };
        }

        public bool MoveNext()
        {
            if (node.NextNode != null)
            {
                node = node.NextNode;
                return true;
            }
            else
                return false;
        }

        public T Current => node.Data;

        object IEnumerator.Current => Current;

        public void Dispose()
        {
        }

        

        public void Reset()
        {
            node = new Node<T> { Data = default(T), NextNode = _head };
        }
    }
}
