﻿using System;
using System.Collections;
using System.Collections.Generic;
using CustomCollection;

namespace COnsole
{
    /// <summary>
    /// Коллекция на основе массива
    /// </summary>
    public class ArrayList<T> : ICustomCollection<T>, IEnumerable
        where T : IComparable<T>
    {
        private T[] array;
        private int size;

        ///<inheritdoc/>
        public void Add(T elem)
        {
            //добавление в пустой список
            if (isEmpty())
            {
                array = new T[1] { elem };
                size = 1;
                return;
            }
            //Список не пустой, место есть
            if (array.Length > size)
            {
                array[size] = elem;
                size += 1; //size = size + 1;
            }
            else
            {
                var newArray = new T[array.Length * 2];
                for (int i = 0; i < size; i++)
                    newArray[i] = array[i];
                newArray[size] = elem;
                size += 1;
                array = newArray;
            }
        }

        ///<inheritdoc/>
        public void AddRange(T[] elems)
        {
            if (isEmpty())
            {
                array = new T[elems.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = elems[i];
                }
                return;

            }
            // список не пустой => добавляем в конец
            if (array.Length > size)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    array[size] = elems[i];
                    size++;
                }
            }
            else
            {
                var newArray = new T[elems.Length * 2];
                for (int i = 0; i < size; i++)
                    newArray[i] = array[i];
                int k = size;
                int j = 0;
                while (j != newArray.Length)
                {
                    newArray[k] = elems[j];
                    k++;
                    j++;
                }

            }
        }

        public void Clear()
        {
            size = 0;
            array = null;
        }

        ///<inheritdoc/>
        public bool Contains(T elem)
        {
            if (isEmpty()) return false;
            else
            {
                int position = 0;
                while (position < size )
                {
                    if (array[position].CompareTo(elem) == 1)
                    {
                        return true;
                    }
                    else position++;
                }
                return false;
                
            }
        }

       

        ///<inheritdoc/>
        public int IndexOf(T elem)
        {
            if (isEmpty()) return 0;
            int position = 0;
            while(array[position].CompareTo(elem) != 0)
            {
                position++;
            }
            return position;
        }

        ///<inheritdoc/>
        public void Insert(int index, T elem)
        {
            if (isEmpty()) return;
            size++;
            int position = 0;

            while(position<=size)
            {

                if(position == index)
                {
                    for(int i = size; i>index; i--)
                    {
                        array[i- 1] = array[i - 2];
                    }
                    array[index-1] = elem;
                }
                position += 1;
            }
            
                 
        }

        public bool isEmpty()
        {
            return size == 0;
        }

        ///<inheritdoc/>
        public void Remove(T elem)
        {
            if (isEmpty()) return;
            var position = 0;
            while(position<size && array[position].CompareTo(elem) != 0)
            {
                position++;
            }
            
            if(position<size-1)
            {
                for(int  i = position + 1; i < size;i++)
                {
                    array[i] = array[i + 1];
                }
                size--;
            }

            if (position < size)
            {
                array[size - 1] = default(T);
                size--;
            }
        }

        ///<inheritdoc/>
        public void RemoveAll(T elem)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Reverse()
        {
            Array.Resize<T>(ref array, size);
            Array.Reverse(array);
        }

        ///<inheritdoc/>
        public int Size()
        {
            return size;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            Array.Resize<T>(ref array, size);
            return array.GetEnumerator();
        }
    }
}

