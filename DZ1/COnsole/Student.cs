﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace COnsole
{
    /// <summary>
    /// Student
    /// </summary>
    public class Student: IComparable<Student>
    {
        public string City { get; set; }
        public string Fio { get; set; }
        public DateTime Birthday { get; set; }
        public float Mark { get; set; }

        public int CompareTo([AllowNull] Student other)
        {
            if (string.Compare(City, other.City, StringComparison.CurrentCulture) == 0)
            {
                if (string.Compare(Fio, other.Fio, StringComparison.CurrentCulture) == 0)
                {
                    if (Birthday.CompareTo(other.Birthday) == 0)
                    {
                        return Mark.CompareTo(other.Mark);
                    }
                    else return Birthday.CompareTo(other.Birthday);
                }
                else return string.Compare(Fio, other.Fio, StringComparison.CurrentCulture);
            }
            else return string.Compare(City, other.City, StringComparison.CurrentCulture);
        }

        public override string ToString()
        {
            return String.Join(", ",
                new[] { City,
                    Fio,
                    Birthday.ToShortDateString(),
                    Mark.ToString() });
        }
    }
}
