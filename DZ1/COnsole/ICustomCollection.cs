﻿using System;
namespace CustomCollection
{
    /// <summary>
    /// Собственная коллекция
    /// </summary>
    public interface ICustomCollection<T>
    {
        /// <summary>
        /// Размер коллекции
        /// </summary>
        /// <returns></returns>
        int Size();

        /// <summary>
        /// Признак пустого списка
        /// </summary>
        /// <returns></returns>
        bool isEmpty();

        /// <summary>
        /// имеется ли элемент в коллекции
        /// </summary>
        /// <returns>имеется ли элемент в коллекции</returns>
        bool Contains(T elem);

        /// <summary>
        /// Добавление элемента в конец
        /// </summary>
        /// <param name="elem"></param>
        void Add(T elem);

        /// <summary>
        /// Добавление в конец нескольких элементов
        /// </summary>
        /// <param name="elems"></param>
        void AddRange(T[] elems);

        /// <summary>
        /// удаление эемента со значением
        /// </summary>
        /// <param name="elems"></param>
        void Remove(T elems);

        /// <summary>
        /// Удаляет все элменеты со значением
        /// </summary>
        /// <param name="elem"></param>
        void RemoveAll(T elem);

        /// <summary>
        /// Удаление элемента на позиции
        /// </summary>
        /// <param name="index"></param>
        void RemoveAt(int index);

        /// <summary>
        /// Очищение коллекции
        /// </summary>
        void Clear();

        /// <summary>
        /// Переворачивает список
        /// </summary>
        void Reverse();

        /// <summary>
        /// Вставка элемента на конкретную позицию
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        void Insert(int index, T item);

        /// <summary>
        /// Возвращает позицию элемента
        /// </summary>
        /// <param name="elem"></param>
        /// <returns></returns>
        int IndexOf(T elem);
    }
}
