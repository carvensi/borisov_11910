﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab13
{
    class Person
    {
        string Name;
        string LastName;
        DateTime BDate;
        char Gender;
        public Person()
        { }
        public Person(string Name, string LastName, DateTime BDate, char Gender)
        { if (Gender == 'М' || Gender == 'Ж')
                {
                this.Name = Name;
                this.LastName = LastName;
                this.BDate = BDate;
                this.Gender = Gender;
            }
        else
            {
                throw new Exception("Пол указан неверно: правильный формат ввода(М/Ж)");
            }
        }
        public void Input()
        {
            string Name;
            string LastName;
            DateTime BDate;
            char Gender;
            Console.WriteLine("Введите имя");
            Name = Console.ReadLine();
            Console.WriteLine("Введите Фамилию");
            LastName = Console.ReadLine();
            Console.WriteLine("Введите дату рождения");
            BDate = Convert.ToDateTime(Console.ReadLine());
            Console.WriteLine("Введите пол");
            Gender = Convert.ToChar(Console.ReadLine());

            {
                if (Gender == 'М' || Gender == 'Ж')
                {
                    this.Name = Name;
                    this.LastName = LastName;
                    this.BDate = BDate;
                    this.Gender = Gender;
                }
                else
                {
                    throw new Exception("Пол указан неверно: правильный формат ввода(М/Ж)");
                }
            }
        }
        public string Output()
        {
            //Console.WriteLine("Имя " + this.Name);
            //Console.WriteLine("Фамилия " + this.LastName);
            //Console.WriteLine("Дата рождения " + this.BDate);
            //Console.WriteLine("Пол " + this.Gender);

            return (Name + "\n" + LastName + "\n" + BDate.Day + "."+ BDate.Month + "." + BDate.Year + "\n" + Gender);
        }
        public int age()
       {
            if(DateTime.Now.Month > BDate.Month || (DateTime.Now.Month == BDate.Month && DateTime.Now.Day > BDate.Day))
            return DateTime.Now.Year - BDate.Year;
            return DateTime.Now.Year - BDate.Year - 1;
       }
        public void Input2(int n)
        {
            Person[] newPersons = new Person[n];
            for(int i = 0;i<n;i++)
            {
                string Name;
                string LastName;
                DateTime BDate;
                char Gender;
                Console.WriteLine("Введите имя");
                Name = Console.ReadLine();
                Console.WriteLine("Введите Фамилию");
                LastName = Console.ReadLine();
                Console.WriteLine("Введите дату рождения");
                BDate = Convert.ToDateTime(Console.ReadLine());
                Console.WriteLine("Введите пол");
                Gender = Convert.ToChar(Console.ReadLine());
                newPersons[i] = new Person(Name, LastName, BDate, Gender);
            }
            for(int i = 0;i<n;i++)
            {
                Console.WriteLine(i+1 + "." + " " + newPersons[i].Name + " " + newPersons[i].LastName + " " + newPersons[i].BDate.Day + "." + newPersons[i].BDate.Month + "." + newPersons[i].BDate.Year + " "+ newPersons[i].Gender);
            }
        }
    }
}
