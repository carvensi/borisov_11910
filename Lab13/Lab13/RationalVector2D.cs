﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab13
{
    class RationalVector2D : RationalFraction
    {
        public RationalVector2D()
        { }
        public RationalVector2D(RationalFraction x, RationalFraction y)
        {
            this.x = x;
            this.y = y;
        }
        protected RationalFraction x;
        protected RationalFraction y;
        public RationalVector2D add(RationalVector2D vector)
        {
            return new RationalVector2D(this.x.add(vector.x),this.y.add(vector.y));
        }
        public string toString1()
        {
            return Convert.ToString("Координата x: " + x.toString() + "\n" + "Координата y: " + y.toString());
        }
        public double length()
        {
            return Math.Sqrt(x.mult(x).add(y.mult(y)).value()); 
        }
        public double cos(RationalVector2D vector)
        {
            return (x.mult(vector.x).add(y.mult(vector.y)).value()) / (this.length() * vector.length());
        }
        public double scalarProduct(RationalVector2D vector)
        {
            return this.length() * vector.length() * this.cos(vector);
        }
        public bool equals(RationalVector2D vector)
        {
            if (this.x.equals(vector.x) && this.y.equals(vector.y)) return true;
            return false;
        }
    }
}
