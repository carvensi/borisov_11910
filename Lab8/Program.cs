﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1");
            StringBuilder word;
            Console.WriteLine("Напишите какое-то слово");
            Input(out word);
            Output(word);

            Console.WriteLine("Задание 2");
            int count;
            StringBuilder word1;
            Console.WriteLine("Напишите какое-то слово");
            Input2(out word1, out count);
            Output2(word1, count);

            Console.WriteLine("Задание 3");
            Console.WriteLine("Напишите какое-то слово");
            StringBuilder word2;
            Input3(out word2);
            Output3(out word2);

            Console.WriteLine("Задание 4");
            StringBuilder word4;
            Console.WriteLine("Напишите какое-то слово");
            Input(out word4);
            Output4(word4);

            Console.WriteLine("Задание 5");
            StringBuilder word5;
            Input5(out word5);
            Output5(word5);

            Console.WriteLine("Задание 6");
            int count6;
            StringBuilder word6;
            Console.WriteLine("Напишите какое-то слово/предложение");
            Input6(out word6, out count6);
            Output6(word6, count6);

            Console.WriteLine("Задание 7");
            string[] count7;
            StringBuilder word7;
            Console.WriteLine("Напишите какое-то предложение");
            Input7(out word7, out count7);
            Output7(word7, count7);

            Console.WriteLine("Задание 8");
            string[] count8;
            StringBuilder word8;
            Console.WriteLine("Напишите какое-то предложение");
            Input8(out word8, out count8);
            Output8(word8, count8);

            Console.WriteLine("Задание 9");
            int count9;
            StringBuilder word9;
            Console.WriteLine("Напишите какое-то предложение");
            Input9(out word9, out count9);
            Output9(word9, count9);

            Console.WriteLine("Задание 10");
            int count10;
            StringBuilder word10;
            Console.WriteLine("Напишите какое-то предложение");
            int n = Input10(out word10, out count10);
            Output10(word10, count10, n);
        }
        static void Resh(StringBuilder word)
        {
            char f;
            for (int i = 0; i < word.Length - 1; i++)
            {
                if (i % 2 == 0)
                {
                    f = word[i + 1];
                    word[i + 1] = word[i];
                    word[i] = f;
                }
            }
        }
        static void Input(out StringBuilder word)
        {
            word = new StringBuilder(Convert.ToString(Console.ReadLine()));
            Resh(word);
        }
        static void Output(StringBuilder word)
        {
            Console.WriteLine(word);
        }
        static void Resh1(StringBuilder word, out int count)
        {
            char f;
            count = 0;
            for (int i = 0; i < word.Length; i++)
            {
                f = word[i];
                if (Char.IsLetter(f))
                {
                    count++;
                }
            }
        }
        static void Input2(out StringBuilder word, out int count)
        {
            word = new StringBuilder(Convert.ToString(Console.ReadLine()));
            Resh1(word, out count);
        }
        static void Output2(StringBuilder word, int count)
        {
            Console.WriteLine("Всего " + count + " букв в этом предложении");
        }
        static bool Resh3(StringBuilder word)
        {
            int count = 0;
            for (int i = 0; i < word.Length - 1; i++)
            {
                if (word[i] == word[i + 1])
                {
                    count++;
                }
            }
            if (count != 0) return true;
            else return false;
        }
        static bool Input3(out StringBuilder word)
        {
            word = new StringBuilder(Convert.ToString(Console.ReadLine()));
            if (Resh3(word))
            {
                return true;
            }
            else return false;
        }
        static void Output3(out StringBuilder word)
        {
            if (Input3(out word))
            {
                Console.Write("Рядом стоящие одинаковык буквы");
            }
            else Console.WriteLine("Рядом стоящих одинаковых букв");
        }

        static void Resh4(StringBuilder word)
        {
            int x = word.Length / 2;
            if (word.Length % 2 == 0)
            {
                word.Remove(x, 1);
                word.Remove(x - 1, 1);
            }
            else
            {
                word.Remove(x, 1);
            }
        }
        static void Output4(StringBuilder word)
        {
            Console.WriteLine("Слово без букв в середине - ");
            Console.WriteLine(word);
        }
        static void Resh5(StringBuilder word, string sl1, string sl2)
        {
            word.Replace(sl1, sl2);
        }
        static void Input5(out StringBuilder word)
        {
            Console.WriteLine("Напишите какое-то слово");
            word = new StringBuilder(Convert.ToString(Console.ReadLine()));
            Console.WriteLine("Буквы для замены");
            string a = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Замена ");
            string b = Convert.ToString(Console.ReadLine());
            Resh5(word, a, b);
        }
        static void Output5(StringBuilder word)
        {
            Console.WriteLine("Слово с заменой - ");
            Console.WriteLine(word);
        }
        static void Resh6(StringBuilder word, out int count)
        {
            char f;
            count = 0;
            for (int i = 0; i < word.Length; i++)
            {
                f = word[i];
                if (Char.IsDigit(f))
                {
                    count += Char.Parse(Convert.ToString(word[i])) - 48;
                }
            }
        }
        static void Input6(out StringBuilder word, out int count)
        {
            word = new StringBuilder(Convert.ToString(Console.ReadLine()));
            Resh6(word, out count);
        }
        static void Output6(StringBuilder word, int count)
        {
            Console.WriteLine("Сумма цифр в этом предложении = " + count);
        }
        static void Resh7(StringBuilder word, out string[] count)
        {
            count = new string[word.Length];
            for (int i = 0; word[i] != ':'; i++)
            {
                count[i] = Convert.ToString(word[i]);
            }
        }
        static void Input7(out StringBuilder word, out string[] count)
        {
            word = new StringBuilder(Convert.ToString(Console.ReadLine()));
            Resh7(word, out count);
        }
        static void Output7(StringBuilder word, string[] count)
        {
            Console.WriteLine("Строка до первого вхождения :");
            for (int i = 0; i < count.Length; i++)
                Console.Write(count[i]);
        }
        static void Resh8(StringBuilder word, out string[] count)
        {
            count = new string[word.Length];

            for (int i = word.Length - 1; word[i] != ':'; i--)
            {
                count[i] = Convert.ToString(word[i]);
            }
        }
        static void Input8(out StringBuilder word, out string[] count)
        {
            word = new StringBuilder(Convert.ToString(Console.ReadLine()));
            Resh8(word, out count);
        }
        static void Output8(StringBuilder word, string[] count)
        {
            Console.WriteLine("Строка после последнего вхождения :");
            for (int i = 0; i < count.Length; i++)
                Console.Write(count[i]);
        }
        static void Resh9(StringBuilder word, out int count)
        {
            count = 0;
            int a1 = -1;
            int a2 = -1;
            for (int i = 0; i < word.Length; i++)
            {
                if (a1 == -1)
                {
                    if (word[i] == ',')
                    {
                        a1 = i;
                    }
                }
                else if (word[i] == ',')
                {
                    a2 = i;
                }
            }
            word.Remove(a1 + 1, word.Length - (word.Length - a2) - a1 - 1);

        }
        static void Input9(out StringBuilder word, out int count)
        {
            word = new StringBuilder(Convert.ToString(Console.ReadLine()));
            Resh9(word, out count);
        }
        static void Output9(StringBuilder word, int count)
        {
            Console.WriteLine("Строка без данных между двумя запятыми - ");
            for (int i = 0; i < word.Length; i++)
            {
                Console.Write(word[i]);
            }
        }
        static int Resh10(StringBuilder word, out int count)
        {
            count = 0;
            char[] f = new char[word.Length];
            for (int i = 0; i < word.Length; i++)
            {
                f[i] = word[i];
            }
            int n = f.Distinct().Count();
            return n;
        }
        static int Input10(out StringBuilder word, out int count)
        {
            word = new StringBuilder(Convert.ToString(Console.ReadLine()));
            return Resh10(word, out count);
        }
        static void Output10(StringBuilder word, int count, int n)
        {
            Console.WriteLine("Количество разных знаков в строке = " + n);
        }
    }
}
