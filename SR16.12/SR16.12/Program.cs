﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SR16._12
{
    class Program
    {
        static void Main(string[] args)
        {
            //Вариант 2

            Console.WriteLine("1 задача");

            string[] Spisok = File.ReadAllLines("input.txt");
            Worker[] workers = new Worker[Spisok.Length];
            for (int i = 0; i < Spisok.Length; i++)
            {
                string Stroka = Spisok[i];
                string[] field = Stroka.Split(';');
                Worker worker = new Worker(field[0], (field[1]), field[2], Convert.ToInt32(field[3]));
                workers[i] = worker;
            }
            Array.Sort(workers);
            Console.WriteLine("Введите должность:");
            string Dolg = Console.ReadLine();
            string[] Saving = new string[workers.Length];
            for (int i = 0; i < Spisok.Length; i++)
            {
                if (workers[i].WorkName == Dolg)
                {
                    Console.WriteLine(workers[i]);
                    Worker worker = workers[i];
                    Saving[i] = worker.ToString();
                }
            }
            File.WriteAllLines("output.txt", Saving);

            Console.WriteLine("2 задача");
            Console.WriteLine("Введите имя первого игрока:");
            Player player1 = new Player(Console.ReadLine(), 50);
            Console.WriteLine("Введите имя второго игрока:");
            Player player2 = new Player(Console.ReadLine(), 50);
            Console.WriteLine("HP каждого игрока равно 50!!!");
            Game game1 = new Game();
            game1.HPEx(player1, player2);

        }
    }
}
