﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR16._12
{
    class Game 
    {
        static int GetRandom()
        {
            Random rnd = new Random();
            int value = rnd.Next(0,3);
            return value;
        }
        public void HPEx(Player player1,Player player2)
        {
            if (Game.GetRandom() == 1)
            {
                Console.WriteLine("Первым Ходит первый игрок " + player1.NickName);
                while (player1.HP > 0 && player2.HP > 0)
                {
                    Console.WriteLine("Ходит первый игрок " + player1.NickName);
                    Console.WriteLine("Выберите силу удара 1 - 9 ");
                    int Ydar = Convert.ToInt32(Console.ReadLine());
                    player2.HP -= Ydar;
                    Console.WriteLine("HP игрока 2 " + player2.NickName + " " + player2.HP);
                    if (player2.HP < 0)
                        break;
                    else
                    {
                        Console.WriteLine("Ходит второй игрок " + player2.NickName);
                        Console.WriteLine("Выберите силу удара 1 - 9");
                        int Ydar2 = Convert.ToInt32(Console.ReadLine());
                        player1.HP -= Ydar2;
                        Console.WriteLine("HP игрока 1 " + player1.NickName + " " + player1.HP);
                        if (player1.HP < 0)
                            break;
                    }
                }
                if (player1.HP == 0)
                    Console.WriteLine("Победил второй игрок " + player2.NickName + " "  + player2.HP);
                else
                    Console.WriteLine("Победил первый игрок " + player1.NickName + " " + player1.HP);
            }
            else
            {
                Console.WriteLine("Первым Ходит второй игрок " + player2.NickName);
                while (player1.HP > 0 && player2.HP > 0)
                {
                    Console.WriteLine("Ходит второй игрок " + player2.NickName);
                    Console.WriteLine("Выберите силу удара 1 - 9");
                    int Ydar = Convert.ToInt32(Console.ReadLine());
                    player1.HP -= Ydar;
                    Console.WriteLine("HP игрока 1 " + player1.NickName + " " + player1.HP);
                    if (player1.HP < 0)
                        break;
                    else
                    {
                        Console.WriteLine("Ходит первый игрок " + player2.NickName);
                        Console.WriteLine("Выберите силу удара 1 - 9");
                        int Ydar2 = Convert.ToInt32(Console.ReadLine());
                        player2.HP -= Ydar2;
                        Console.WriteLine("HP игрока 2 " + player2.NickName + " " + player2.HP);
                        if (player2.HP < 0)
                            break;
                    }
                }
                if (player1.HP == 0)
                    Console.WriteLine("Победил второй игрок " + player2.NickName + " " + player2.HP);
                else
                    Console.WriteLine("Победил первый игрок " + player1.NickName + " " + player1.HP);
            }
        }
    }
}
