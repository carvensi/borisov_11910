﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR16._12
{
    class Worker : IComparable
    {
        public string LastName { get; private set; }
        public string Name { get; private set; }
        public string WorkName { get; private set; }
        public int Year { get; private set; }
       public Worker(string lastname,string name,string workname, int year)
        {
            LastName = lastname;
            Name = name;
            WorkName = workname;
            Year = year;
        }
        public int CompareTo(object obj)
        {
            return LastName.CompareTo(((Worker)obj).LastName);
        }
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}",
                                 LastName, Name, WorkName, Year);
        }


    }
}
