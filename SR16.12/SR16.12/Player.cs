﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SR16._12
{
    class Player
    {
        public string NickName { get;  set; }
        public int HP { get; set; }
        public Player (string nick,int hp)
        {
            NickName = nick;
            HP = hp;
        }

    }
}
