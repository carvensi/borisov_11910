﻿using System;
using System.Collections.Generic;
using System.Text;
using CustomCollection;

namespace ConsoleApp1
{
    /// <summary>
    /// Для запуска примеров
    /// </summary>
    public class Runner
    {
        public void Run() 
        {
            var list = new CustomList<int>();
            list.AddRange(new []{ 4,2,5,6,15,3,5,2});
            //list.AddRange(new []{ 4,2,5,6,15,3,2});

            list.Remove(5);
            list.Reverse();
            Console.WriteLine(list.Size());
            Console.WriteLine(list.ToString());
            list.RemoveAt(4);
            Console.WriteLine(list.Size());
            Console.WriteLine(list.ToString());
            if(list.Contains(2))Console.WriteLine("YES");
            else Console.WriteLine("NOOOO");
        }
    }
}
