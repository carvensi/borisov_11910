﻿using System;
using CustomCollection;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var runner = new Runner();
            runner.Run();
        }  
    }
}
