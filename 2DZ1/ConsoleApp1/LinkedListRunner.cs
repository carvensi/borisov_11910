﻿using System;
using System.Collections.Generic;
using System.Text;
using CustomCollection;

namespace ConsoleApp1
{
    /// <summary>
    /// Примеры для двунаправленного связного списка
    /// </summary>
    public class LinkedListRunner
    {
        public void Run() {
            var list = new CustomLinkedList<int>();
            list.Add(6);
            list.Add(7);
            list.Add(14);
            list.Add(9);
            list.Add(33);

            Console.WriteLine(list.ToString());
        }
    }
}
