﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    public class CustomList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        /// <summary>
        /// Головной элемент
        /// </summary>
        private Node<T> head;

        ///<inheritdoc/>
        public void Add(T elem)
        {
            if (head == null)
                head = new Node<T>() { Data = elem };
            else
            {
                var node = head;
                while (node.NextNode != null)
                    node = node.NextNode;
                node.NextNode = new Node<T>() { Data = elem };
            }
        }
        ///<inheritdoc/>
        public void AddRange(T[] elems)
        {
            var arr = elems;
            for (int i = 0; i < arr.Length; i++)
            {
                if (head == null)
                    head = new Node<T>() { Data = elems[i] };
                else
                {
                    var node = head;
                    while (node.NextNode != null)
                        node = node.NextNode;
                    node.NextNode = new Node<T>() { Data = elems[i] };
                }
            }
        }

        public void Clear()
        {
            head = null;
        }
        ///<inheritdoc/>
        public bool Contains(T elem)
        {
            var node = head;
            do
            {
                if (node.Data.CompareTo(elem) == 0)
                {
                    return true;
                }
                node = node.NextNode;
            } while (node != null);

            return false;
        }
        ///<inheritdoc/>
        public int IndexOf(T elem)
        {
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public void Insert(int index, T elem)
        {
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public bool isEmpty()
        {
            return head == null;
        }
        ///<inheritdoc/>
        public void Remove(T elem)
        {
            if (head != null)
            {
                if (head.Data.Equals(elem))
                {
                    head = head.NextNode;
                }
                var node = head.NextNode;
                var prev = head;
                while (node.NextNode != null)
                {
                    if (node.NextNode.Data.Equals(elem))
                    {
                        prev = node.NextNode;
                    }
                }
            }
        }
        ///<inheritdoc/>
        public void RemoveAll(T elem)
        {
            if (isEmpty()) return;
            var node = head;
            Node<T> prevNode = null;
            while (node != null)
            {
                if (node.Data.CompareTo(elem) != 0)
                {
                    prevNode = node;
                    node = node.NextNode;
                }
                else
                {
                    if (prevNode == null) //головной элемент
                    {
                        head = head.NextNode;
                        node = head.NextNode;
                    }
                    else
                    {
                        prevNode.NextNode = node.NextNode;
                        node = node.NextNode;
                    }
                }
            }
        }
        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            //todo домой
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public void Reverse()
        {
            Node<T> node;
            if (isEmpty()) return;
            node = new Node<T>() { Data = head.Data };
            while (head != null)
            {
                head = head.NextNode;
                if (isEmpty())
                {
                    head = node;
                    return;
                }
                node = new Node<T>() { Data = head.Data, NextNode = node };
            }
        }
        ///<inheritdoc/>
        public int Size()
        {
            //todo домой
            throw new NotImplementedException();
        }

        /// <summary>
        /// Вывести список строкой
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (head == null) return null;
            var node = head;
            var sb = new StringBuilder();
            while (node != null)
            {
                sb.Append(" " + node.Data.ToString());
                node = node.NextNode;
            }
            return sb.ToString();
        }
    }
}

