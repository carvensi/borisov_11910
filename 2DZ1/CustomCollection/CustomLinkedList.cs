﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    /// <summary>
    /// Двунаправленный список
    /// </summary>
    public class CustomLinkedList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        private LinkedNode<T> head;

        ///<inheritdoc/>
        public void Add(T elem)
        {
            if (head == null)
                head = new LinkedNode<T>() { Data = elem };
            else
            {
                var node = head;
                while (node.NextNode != null)
                {
                    node = node.NextNode;
                }
                var newNode = new LinkedNode<T>() { Data = elem };
                node.NextNode = newNode;
                newNode.PrevNode = node;
            }
        }

        ///<inheritdoc/>
        public void AddRange(T[] elems)
        {
            var node = head;
            while (node.NextNode != null)
            {
                node = node.NextNode;
            }
            for (int i = 0; i < elems.Length; i++)
            {
                var newNode = new LinkedNode<T>() { Data = elems[i] };
                node.NextNode = newNode;
            }
        }

        ///<inheritdoc/>
        public void Clear()
        {
            head = null;
        }

        ///<inheritdoc/>
        public bool Contains(T elem)
        {
            bool result = false;
            var node = head;
            while (node.NextNode != null)
            {
                if (node.Data.Equals(elem))
                {
                    result = true;
                    break;
                }
                else
                    node = node.NextNode;
            }
            return result;
        }

        ///<inheritdoc/>
        public int IndexOf(T elem)
        {
            int count = 1;
            var node = head;
            while (node.NextNode != null)
            {
                if (node.Data.Equals(elem))
                    return count;
                else
                    node = node.NextNode;
                count++;
            }
            return -1;
        }

        ///<inheritdoc/>
        public void Insert(int index, T elem)
        {
            int count = 0;
            if (head != null)
            {
                var node = head;
                var prev = head.PrevNode;
                while (node != null)
                {
                    if (count != index)
                    {
                        prev.NextNode = node.NextNode;
                    }
                    prev = node;
                    node = node.NextNode;
                }
            }
        }

        ///<inheritdoc/>
        public bool isEmpty()
        {
            //todo домой
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Remove(T elem)
        {
            if (head != null)
            {
                if (head.Data.Equals(elem))
                {
                    head = head.NextNode;
                }
                var node = head.NextNode;
                var prev = head;
                while (node != null)
                {
                    if (node.Data.Equals(elem))
                    {
                        prev.NextNode = node.NextNode;
                    }
                    prev = node;
                    node = node.NextNode;
                }
            }
        }

        ///<inheritdoc/>
        public void RemoveAll(T elem)
        {

        }

        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Reverse()
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public int Size()
        {
            if (head == null) return 0;
            var node = head;
            int count = 1;
            while (node.NextNode != null)
            {
                count++;
                node = node.NextNode;
            }
            return count;
        }

        /// <summary>
        /// Вывести список строкой
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (head == null) return null;
            var node = head;
            var sb = new StringBuilder();
            while (node != null)
            {
                sb.Append(" " + node.Data.ToString());
                node = node.NextNode;
            }
            return sb.ToString();
        }
    }
}
