﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Lab11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1");
            Console.WriteLine("Введите сообщение(Дата вида ДД-ММ-ГГГГ):");
            string soob = Console.ReadLine();
            Regex regex = new Regex(@"(\d{2}-\d{2}-\d{2})|(\d{3}-\d{3})|(\d{3}-\d{2}-\d{2})");
            MatchCollection matches = regex.Matches(soob);
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                    Console.WriteLine(match.Value);
            }
            else
            {
                Console.WriteLine("Номера телефонов не найдены");
            }

            Console.WriteLine("Задание 2");
            Regex regex2 = new Regex("([0-2][0-9]|[3][0-1]).([0][0-9]|[1][0-2]).([1][9,0][0-9][0-9]|[2][0][0-9][0]|[2][0][1][0])");
            MatchCollection matches2 = regex2.Matches(soob);
            if (matches2.Count > 0)
            {
                Console.WriteLine("Даты:");
                for (int i = 0; i < matches2.Count; i++)
                {
                    string updDate = DateTime.Parse(matches2[i].Value).ToShortDateString();
                    Console.WriteLine(updDate);
                }
            }
            else
                Console.WriteLine("Дат не найдено");

            Console.WriteLine("Задание 3");
            int d = -1;
            Regex regex3 = new Regex(@"([0-9][0-9]|[1][0-9][0-9]|[2][0-9][0-5])\.([0-9][0-9]|[1][0-9][0-9]|[2][0-9][0-5])\.([0-9][0-9]|[1][0-9][0-9]|[2][0-9][0-5])\.([0-9][0-9]|[1][0-9][0-9]|[2][0-9][0-5])");
            MatchCollection matches3 = regex3.Matches(soob);
            if (matches3.Count > 0)
            {
                Console.WriteLine("Удаление IP адресов");
                while (d < 1 || d > 9)
                {
                    Console.Write("Введите первую цифру последнего числа IP адреса: ");
                    d = Convert.ToInt32(Console.ReadLine());
                }

                string endPattern = "";
                if (d == 1)
                    endPattern = @"(1[0-9]{2}|1[0-9]|1)";
                else if (d == 2)
                    endPattern = @"(25[0-5]|2[0-4][0-9]|2[0-9]|2)";
                else
                    endPattern = d.ToString();

                string pattern = @"([0-9][0-9]|[1][0-9][0-9]|[2][0-9][0-5])\.([0-9][0-9]|[1][0-9][0-9]|[2][0-9][0-5])\.([0-9][0-9]|[1][0-9][0-9]|[2][0-9][0-5])\." + endPattern;
                Regex newReg = new Regex(pattern);
                string newText = newReg.Replace(soob, "");
                Console.WriteLine(newText);
            }
            else
            {
                Console.WriteLine("IP адресов не найдено");
            }

            Console.WriteLine("Задание 4");
            Regex regex4 = new Regex(@"((https?|ftp)\:\/\/)?([a-z0-9]{1})((\.[a-z0-9-])|([a-z0-9-]))*\.([a-z]{2,6})(\/?)");
            MatchCollection matches4 = regex4.Matches(soob);
            if (matches4.Count > 0)
            {
                Console.WriteLine("Все адреса вебсайтов:");
                foreach (Match match in matches4)
                    Console.WriteLine(match.Value);
            }
            else
            {
                Console.WriteLine("WEB адресов не найдено");
            }

            Console.WriteLine("Задание 5");
            Regex regex6 = new Regex("[0-3][0-9].[0-1][0-9].[1,2][9,0][0-9][0-9]");
            MatchCollection matches6 = regex6.Matches(soob);
            if (matches6.Count > 0)
            {
                for (int i = 0; i < matches6.Count; i++)
                {
                    string updDate = DateTime.Parse(matches6[i].Value).AddDays(1).ToShortDateString();
                    soob = soob.Replace(matches6[i].Value, updDate);
                }
                Console.WriteLine(soob);
            }
            else
                Console.WriteLine("Дат не найдено");
        }
    }
}
