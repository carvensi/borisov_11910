﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab6
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Задание 1");
            Console.WriteLine("Массив масс 20 людей:");
            Mass();

            Console.WriteLine("Задание 2");
            Console.WriteLine("Введите длину массива:");
            int n = Convert.ToInt32(Console.ReadLine());
            
            int[] chisl = new int[n];
            Input2(n,chisl);
            Resh2(n,chisl);
            Output2(n,chisl);

            Console.WriteLine("Задание 3");
            Console.WriteLine("Введите длину массива:");
            int n3 = Convert.ToInt32(Console.ReadLine());
            int[] chisl2 = new int[n3];
            Input2(n3,chisl2);
            Resh3(n3,chisl2);

            Console.WriteLine("Задание 4");
            Console.WriteLine("Введите длину массива:");
            int n4 = Convert.ToInt32(Console.ReadLine());
            int[] chisl3 = new int[n4];
            Input2(n4,chisl3);
            Resh4(n4,chisl3);

            Console.WriteLine("Задание 5");
            Console.WriteLine("Введите длину массива:");
            int n5 = Convert.ToInt32(Console.ReadLine());
            int[] chisl4 = new int[n5];
            Input2(n5,chisl4);
            Resh5(n5,chisl4);
        }
        static void Mass()
        {
            int[] ves = new int[20];
            Random rnd = new Random();
            for (int i=0;i<20;i++)
            {
                ves[i] = rnd.Next(50,101);
                Console.WriteLine(ves[i]);
            }
        }
        static void Input2(int a,int[] chisl)
        {
            for (int i = 0;i<a;i++)
            {
                Console.WriteLine("Введите " + (i+1) + " элемент массива");
                chisl[i] = Convert.ToInt32(Console.ReadLine());
            }
        }
        static void Resh2(int a,int[] chisl)
        {
            Console.WriteLine("Введите номер действия(4 - выполнить все действия):");
            int c = Convert.ToInt32(Console.ReadLine());
            switch (c)
            {
                case 1:
                    for (int i = 0; i < a; i++)
                    {
                        chisl[i] = chisl[i] * 2;
                    }
                    break;
                case 2:
                    int num = chisl[0];
                    for (int i = 0; i < a; i++)
                    {
                        chisl[i] = chisl[i] / num;
                    }
                    break;
                case 3:
                    Console.WriteLine("Введите число A:");
                    int a1 = Convert.ToInt32(Console.ReadLine());
                    for (int i = 0; i < a; i++)
                    {
                        chisl[i] = chisl[i]-a1;
                    }
                    break;
                case 4:
                    int num1 = chisl[0];
                    Console.WriteLine("Введите число A:");
                    int a2 = Convert.ToInt32(Console.ReadLine());
                    for (int i = 0; i < a; i++)
                    {
                        chisl[i] = chisl[i]*2;
                        chisl[i] = chisl[i] / num1;
                        chisl[i] = chisl[i] - a2;
                    }
                    break;
                default:
                    Console.WriteLine("Введено неверное значение");
                    break;
            }
        }
        static void Resh3(int a,int[]chisl2)
        {
            int z = 0;
            int z1 = 0;
            for (int i= 0;i<a-1;i++)
            {
                z = chisl2[i] + chisl2[i + 1];
                z1 = Convert.ToInt32(Math.Pow(chisl2[i], 2))+ Convert.ToInt32(Math.Pow(chisl2[i+1], 2));
            }
            if (z %2 == 0)
            {
                Console.WriteLine("Сумма элементов четное число");
            }
            else
            {
                Console.WriteLine("Сумма элементов НЕ четное число");
            }
            if ((z1>9999)&&(z1<100000))
            {
                Console.WriteLine("Сумма квадратов элементов пятизначное число");
            }
            else
            {
                Console.WriteLine("Сумма квадратов элементов НЕ пятизначное число");
            }
        }
        static void Resh4(int a,int[]chisl3)
        {
            int b = 0;
            int b1 = 0;
            for (int i = 0; i < a; i++)
            {
                if (i%2==0)
                {
                    b = chisl3[i] + b;
                }
                else
                {
                    b1 = chisl3[i] + b1;
                }
            }
            if (b == b1)
            {
                Console.WriteLine("Одинаковое количество людей на той и другой стороне");
            }
            else
            {
                if (b < b1)
                {
                    Console.WriteLine("Людей живет больше на четной стороне");
                }
                else
                {
                    Console.WriteLine("Людей живет больше на нечетной стороне");
                }
            }
        }
        static void Resh5(int a,int[]chisl4)
        {
            int k = 0;
            for (int i = 0; i < a-1; i++)
            {
                if (((chisl4[i]>chisl4[i+1])&&(chisl4[i+1]<0 && chisl4[i]>0))|| ((chisl4[i] < chisl4[i + 1]) && (chisl4[i + 1] > 0 && chisl4[i] < 0)))
                {
                    k++;
                }
            }
            Console.WriteLine("Знак меняется " + k + " раз(a)");
        }
        static void Output2(int a,int[] chisl)
        {
            for (int i = 0; i < a; i++)
            {
                Console.WriteLine(chisl[i]);
            }
        }

    }
}
