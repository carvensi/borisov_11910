﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ЗАДАЧИ I ");

            Console.WriteLine("Задача 1 ");

            Console.WriteLine("Введите периметр: ");
            double p = Convert.ToDouble(Console.ReadLine());
            double s;
            s = (Math.Sqrt(3) / Convert.ToDouble(4)) * (Math.Pow((p / 3),2));
            Console.WriteLine("Площадь равностороннего треугольника равна: " + s);

            Console.WriteLine("Задача 2 ");

            Console.WriteLine("Введите два числа: ");
            double a = Convert.ToDouble(Console.ReadLine());
            double b = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Среднее арифмитическое: " + ((Math.Pow(a, 3) + Math.Pow(b, 3)) / 2));

            Console.WriteLine("Задача 3 ");

            Console.WriteLine("Введите катеты: ");
            double Kat1 = Convert.ToDouble(Console.ReadLine());
            double Kat2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Гипотенуза равна: " + (Math.Sqrt(Math.Pow(Kat1,2)+Math.Pow(Kat2,2))));

            Console.WriteLine("Задача 4 ");

            Console.WriteLine("Введите катеты: ");
            double kat1 = Convert.ToDouble(Console.ReadLine());
            double kat2 = Convert.ToDouble(Console.ReadLine());
            double kat3 = (Math.Sqrt(Math.Pow(Kat1, 2) + Math.Pow(Kat2, 2)));
            Console.WriteLine("Периметр прямоугольного треугольника равен: " + (kat1+kat2+kat3));

            Console.WriteLine("Задача 5 ");

            Console.WriteLine("Введите обьем куба: ");
            int V = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ребро куба: " + Math.Pow(V,1.0/3));

            Console.WriteLine("Задача 6 ");

            Console.WriteLine("Введите координаты вершин x1,x2,y1,y2,x3,x3,y3 ");
            int x1 = Convert.ToInt32(Console.ReadLine());
            int x2 = Convert.ToInt32(Console.ReadLine());
            int y1 = Convert.ToInt32(Console.ReadLine());
            int y2 = Convert.ToInt32(Console.ReadLine());
            int x3 = Convert.ToInt32(Console.ReadLine());
            int y3 = Convert.ToInt32(Console.ReadLine());
            double st1 = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
            double st2 = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y3 - x3, 2));
            double st3 = Math.Sqrt(Math.Pow(y3 - x1, 2) + Math.Pow(y2 - y1, 2));
            Console.WriteLine("Периметр равен:" + st1 + st2 + st3);

            Console.WriteLine("Задача 7 ");

            Console.WriteLine("Введите длину окружности:  ");
            int l = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Радиус окружности равен: " + (Math.PI * 2 / l));

            Console.WriteLine("Задача 8 ");

            Console.WriteLine("Введите основание 1 и основание 2, угол при большем основании:  ");
            int osn1 = Convert.ToInt32(Console.ReadLine());
            int osn2 = Convert.ToInt32(Console.ReadLine());
            int alpha = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Площадь равнобедренной трапеции равна: " + (0.5 * (Math.Pow(osn2, 2) - Math.Pow(osn1, 2) * Math.Tan(alpha * Math.PI / 180D))));

            Console.WriteLine("Задача 9 ");

            Console.WriteLine("Введите сторону треугольника:  ");
            int storonaA = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Радиус окружности равен: " + 0.5 * 3 * storonaA);

            Console.WriteLine("Задача 10 ");

            Console.WriteLine("Введите первый член, разность и число членов прогрессии:  ");
            int pervCH = Convert.ToInt32(Console.ReadLine());
            int raz = Convert.ToInt32(Console.ReadLine());
            int chisloCh = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Сумма членов арифметической прогрессии: " +(2 * pervCH + (raz - 1) * chisloCh) / 2 * raz);


            Console.WriteLine("ЗАДАЧИ II ");

            Console.WriteLine("Задача 1 ");

            Console.WriteLine("Введите число: ");
            int chislo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(chislo % 2 == 0 ? "Является" : "Не является");

            Console.WriteLine("Задача 2 ");

            Console.WriteLine("Введите числа: ");
            int M = Convert.ToInt32(Console.ReadLine());
            int N = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(M % N == 0 ? "Частное от деления: " + Convert.ToString(M / N) : "M не делится нацело на N");

            Console.WriteLine("Задача 3 ");

            Console.WriteLine("Введите число: ");
            int chislo7 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(chislo7 % 10 == 7 ? "Оканчивается" : "Не оканчивается");

            Console.WriteLine("Задача 4 ");

            Console.WriteLine("Введите число: ");
            int doublechislo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(doublechislo % 10 > doublechislo / 10 ? "Второе" : "Первое");

            Console.WriteLine("Задача 5 ");

            Console.WriteLine("Введите число: ");
            int doublechislo2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(doublechislo2 % 10 == doublechislo2 / 10 ? "Одинаковы" : "Неодинаковы");

            Console.WriteLine("Задача 6 ");

            Console.WriteLine("Введите стороны треугольника соответственно a b c: ");
            int stor1 = Convert.ToInt32(Console.ReadLine());
            int stor2 = Convert.ToInt32(Console.ReadLine());
            int stor3 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(stor1 >= stor3 + stor2 || stor3 >= stor2 + stor1 || stor2 >= stor3 + stor1 ? "Не существует" : "Существует");

        }
    }
}
