﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AiSD
{
    public static class Utils
    {
        static public int Array(int[] array)
        {
            int count = 0;
            for (int i = 0; i < array.Length; i++)
            {
                bool result = false;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] != array[j])
                        result = true;
                    else
                    {
                        result = false;
                        break;
                    }
                }
                if (result)
                    count++;
            }
            count++; // потому что последний элемент всегда уникален
            return count;
        }

    }
}
