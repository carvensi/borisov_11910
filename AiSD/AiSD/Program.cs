﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AiSD
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 1, 3, 5, 3, 7, 3 };
            Console.WriteLine(Utils.Array(array));
            Point[] points = new Point[array.Length * array.Length];
            int count = 0;
            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < array.Length; j++)
                {
                    points[count] = new Point(i, array[i]);
                    count++;
                }
            }
            Console.WriteLine(Point.IsoscelesTriangles(points));
        }
    }
}
