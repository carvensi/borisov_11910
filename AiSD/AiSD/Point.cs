﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AiSD
{
    public class Point
    {
        int x { get; set; }
        int y { get; set; }
        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
        public static double Length(Point a, Point b)
        {
            return Math.Sqrt(Math.Pow((a.x - b.x), 2) + Math.Pow((a.y - b.y), 2));
        }
        public static int IsoscelesTriangles(Point[] points)
        {
            int count = 0;
            for (int i = 0; i < points.Length; i++)
            {
                for (int j = 0; j < points.Length - 1; j++)
                {
                    if (i == j) continue;
                    Point[] triangle = { points[i], points[j], points[j + 1] };
                    double a = Point.Length(points[i], points[j]);
                    double b = Point.Length(points[i], points[j + 1]);
                    double c = Point.Length(points[j], points[j + 1]);
                    if ((a == b) && (b == c) || (a == b) && (a == c) || (b == c) && (a == c)) count++;
                    else continue;
                }
            }
            return count++;
        }
    }
}
